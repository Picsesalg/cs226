/**
 * Alice Yang
 * ayang36@jhu.edu
 * BalancedBSTBench.java
 */

import com.github.phf.jb.Bench;
import com.github.phf.jb.Bee;

import java.lang.Math;

public class BalancedBSTBench {

    private static final int SIZE = 100;

    private static void insertLinear(AvlTreeMap<Integer, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            m.insert(i, i);
        }
    }

    private static void insertRandom(AvlTreeMap<Integer, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            int a = (int) Math.random() * SIZE;
            if (!m.has(a)) {
                m.insert(a, i);
            }
        }
    }

    private static void removeLinear(AvlTreeMap<Integer, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            if (m.has(i)) {
                m.remove(i);
            }
        }
    }
    
    private static void removeRandom(AvlTreeMap<Integer, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            int a = (int) Math.random() * SIZE;
            if (m.has(a)) {
                m.remove(a);
            }
        }
    }

    private static void lookupLinear(AvlTreeMap<Integer, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            boolean b = m.has(i);
        }
    }

    private static void lookupRandom(AvlTreeMap<Integer, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            boolean b = m.has((int) Math.random() * SIZE);
        }
    }


    //Actual benchmarks to test now

    @Bench
    public void insertLinearBST(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            AvlTreeMap<Integer, Integer> m = new AvlTreeMap<Integer, Integer>();
            b.start();
            insertLinear(m);
        }
    }

    @Bench
    public void insertRandomBST(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            AvlTreeMap<Integer, Integer> m = new AvlTreeMap<Integer, Integer>();
            b.start();
            insertRandom(m);
        }
    }

    @Bench
    public void insertLinearRemoveLinearBST(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            AvlTreeMap<Integer, Integer> m = new AvlTreeMap<Integer, Integer>();
            insertLinear(m);
            b.start();
            removeLinear(m);
        }
    }

    @Bench
    public void insertLinearRemoveRandomBST(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            AvlTreeMap<Integer, Integer> m = new AvlTreeMap<Integer, Integer>();
            insertLinear(m);
            b.start();
            removeRandom(m);
        }
    }

    @Bench
    public void insertRandomRemoveLinearBST(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            AvlTreeMap<Integer, Integer> m = new AvlTreeMap<Integer, Integer>();
            insertRandom(m);
            b.start();
            removeLinear(m);
        }
    }

    @Bench
    public void insertRandomRemoveRandomBST(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            AvlTreeMap<Integer, Integer> m = new AvlTreeMap<Integer, Integer>();
            insertRandom(m);
            b.start();
            removeRandom(m);
        }
    }

    @Bench
    public void insertLinearLookUpLinearBST(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            AvlTreeMap<Integer, Integer> m = new AvlTreeMap<Integer, Integer>();
            insertLinear(m);
            b.start();
            lookupLinear(m);
        }
    }

    @Bench
    public void insertLinearLookUpRandomBST(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            AvlTreeMap<Integer, Integer> m = new AvlTreeMap<Integer, Integer>();
            insertLinear(m);
            b.start();
            lookupRandom(m);
        }
    }

    @Bench
    public void insertRandomLookUpLinearBST(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            AvlTreeMap<Integer, Integer> m = new AvlTreeMap<Integer, Integer>();
            insertRandom(m);
            b.start();
            lookupLinear(m);
        }
    }

    @Bench
    public void insertRandomLookUpRandomBST(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            AvlTreeMap<Integer, Integer> m = new AvlTreeMap<Integer, Integer>();
            insertRandom(m);
            b.start();
            lookupRandom(m);
        }
    }
}
