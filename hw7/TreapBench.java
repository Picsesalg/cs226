/**
 * Alice Yang
 * ayang36@jhu.edu
 * BalancedBSTBench.java
 */

import com.github.phf.jb.Bench;
import com.github.phf.jb.Bee;

import java.lang.Math;

public class TreapBench {

    private static final int SIZE = 100;

    private static void insertLinear(TreapMap<Integer, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            m.insert(i, i);
        }
    }

    private static void insertRandom(TreapMap<Integer, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            int a = (int) Math.random() * SIZE;
            if (!m.has(a)) {
                m.insert(a, i);
            }
        }
    }

    private static void removeLinear(TreapMap<Integer, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            if (m.has(i)) {
                m.remove(i);
            }
        }
    }
    
    private static void removeRandom(TreapMap<Integer, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            int a = (int) Math.random() * SIZE;
            if (m.has(a)) {
                m.remove(a);
            }
        }
    }

    private static void lookupLinear(TreapMap<Integer, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            boolean b = m.has(i);
        }
    }

    private static void lookupRandom(TreapMap<Integer, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            boolean b = m.has((int) Math.random() * SIZE);
        }
    }


    //Actual benchmarks to test now

    @Bench
    public void insertLinearBST(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            TreapMap<Integer, Integer> m = new TreapMap<Integer, Integer>();
            b.start();
            insertLinear(m);
        }
    }

    @Bench
    public void insertRandomBST(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            TreapMap<Integer, Integer> m = new TreapMap<Integer, Integer>();
            b.start();
            insertRandom(m);
        }
    }

    @Bench
    public void insertLinearRemoveLinearBST(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            TreapMap<Integer, Integer> m = new TreapMap<Integer, Integer>();
            insertLinear(m);
            b.start();
            removeLinear(m);
        }
    }

    @Bench
    public void insertLinearRemoveRandomBST(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            TreapMap<Integer, Integer> m = new TreapMap<Integer, Integer>();
            insertLinear(m);
            b.start();
            removeRandom(m);
        }
    }

    @Bench
    public void insertRandomRemoveLinearBST(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            TreapMap<Integer, Integer> m = new TreapMap<Integer, Integer>();
            insertRandom(m);
            b.start();
            removeLinear(m);
        }
    }

    @Bench
    public void insertRandomRemoveRandomBST(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            TreapMap<Integer, Integer> m = new TreapMap<Integer, Integer>();
            insertRandom(m);
            b.start();
            removeRandom(m);
        }
    }

    @Bench
    public void insertLinearLookUpLinearBST(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            TreapMap<Integer, Integer> m = new TreapMap<Integer, Integer>();
            insertLinear(m);
            b.start();
            lookupLinear(m);
        }
    }

    @Bench
    public void insertLinearLookUpRandomBST(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            TreapMap<Integer, Integer> m = new TreapMap<Integer, Integer>();
            insertLinear(m);
            b.start();
            lookupRandom(m);
        }
    }

    @Bench
    public void insertRandomLookUpLinearBST(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            TreapMap<Integer, Integer> m = new TreapMap<Integer, Integer>();
            insertRandom(m);
            b.start();
            lookupLinear(m);
        }
    }

    @Bench
    public void insertRandomLookUpRandomBST(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            TreapMap<Integer, Integer> m = new TreapMap<Integer, Integer>();
            insertRandom(m);
            b.start();
            lookupRandom(m);
        }
    }
}
