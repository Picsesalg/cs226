/**
 * Alice Yang
 * ayang36@jhu.edu
 * TreapMapTest.java
 */

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.assertEquals;

public class TreeMapTest {

    private TreapMap<Integer, Integer> a;
    
    @Before
    public void setUp() {
        a = new TreapMap<Integer, Integer>();
    }

    @Test
    public void testSize01() {
        a.insert(2, 1);
        a.insert(1, 2);
        a.insert(3, 3);
        assertEquals(3, a.size());
    }

    @Test
    public void testSize02() {
        assertEquals(0, a.size());
    }

    @Test
    public void testSize03() {
        a.insert(2, 1);
        a.insert(1, 2);
        a.insert(-232, 4);
        a.insert(3, 3);
        a.insert(10232, 7);
        a.remove(3);
        a.remove(2);
        assertEquals(3, a.size());
    }

    @Test
    public void testHas01() {
        a.insert(2, 1);
        a.insert(1, 2);
        a.insert(-232, 4);
        a.insert(3, 3);
        a.insert(10232, 7);
        assertEquals(true, a.has(-232));
    }

    @Test
    public void testHas03() {
        a.insert(2, 1);
        a.insert(1, 2);
        a.insert(-232, 4);
        a.insert(3, 3);
        a.insert(10232, 7);
        assertEquals(false, a.has(4));
    }

    @Test
    public void testHas04() {
        a.insert(2, 1);
        a.insert(1, 2);
        a.insert(-232, 4);
        a.insert(3, 3);
        a.insert(10232, 7);
        a.remove(3);
        assertEquals(true, a.has(-232));
    }

    @Test
    public void testHas05() {
        a.insert(2, 1);
        a.insert(1, 2);
        a.insert(-232, 4);
        a.insert(3, 3);
        a.insert(10232, 7);
        a.remove(3);
        assertEquals(false, a.has(3));
    }

    @Test
    public void testGet01() {
        a.insert(5, 1);
        assertEquals((Integer) 1, a.get(5));
    }

    @Test(expected=IllegalArgumentException.class)
    public void testGetException() {
        a.insert(5, 1);
        a.get(6);
    }

    @Test 
    public void testPut01() {
        a.insert(5, 1);
        a.put(5, 2);
        assertEquals((Integer) 2, a.get(5));
    }

    @Test(expected=IllegalArgumentException.class)
    public void testPutException() {
        a.insert(5, 1);
        a.get(6);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testInsertException01() {
        a.insert(5, 1);
        a.insert(5, 2);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testRemoveException01() {
        a.insert(5, 1);
        a.remove(6);
    }
}
