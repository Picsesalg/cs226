/**
 * Alice Yang
 * ayang36@jhu.edu
 * TestAvlTreeMap.java
 */

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.assertEquals;

public class AvlTreeMapTest {

    AvlTreeMap<Integer, Integer> a;

    @Before
    public void setUp() {
        a = new AvlTreeMap<Integer, Integer>();
    }

    @Test
    public void simpleInsert01() {
        a.insert(5, 1);
        a.insert(4, 2);
        a.insert(6, 3);
        String s = "{4: 1, 5: 2, 6: 1}";
        assertEquals(s, a.toString());
    }

    @Test
    public void leftLeftInsert01() {
        a.insert(10, 1);
        a.insert(9, 2);
        a.insert(8, 3);
        String s = "{8: 1, 9: 2, 10: 1}";
        assertEquals(s, a.toString());
    }

    @Test
    public void leftRightInsert01() {
        a.insert(20, 1);
        a.insert(10, 2);
        a.insert(30, 3);
        a.insert(5, 4);
        a.insert(15, 5);
        a.insert(12, 6);
        String s = "{5: 1, 10: 2, 12: 1, 15: 3, 20: 2, 30: 1}";
        assertEquals(s, a.toString());
    }

    @Test
    public void rightRightInsert01() {
        a.insert(20, 1);
        a.insert(10, 2);
        a.insert(30, 3);
        a.insert(25, 4);
        a.insert(40, 5);
        a.insert(45, 6);
        String s = "{10: 1, 20: 2, 25: 1, 30: 3, 40: 2, 45: 1}";
        assertEquals(s, a.toString());
    }

    @Test
    public void rightLeftInsert01() {
        a.insert(20, 1);
        a.insert(10, 2);
        a.insert(30, 3);
        a.insert(25, 4);
        a.insert(45, 5);
        a.insert(22, 6);
        String s = "{10: 1, 20: 2, 22: 1, 25: 3, 30: 2, 45: 1}";
        assertEquals(s, a.toString());
    }

    @Test
    public void simpleRemove01() {
        a.insert(5, 1);
        a.insert(4, 2);
        a.insert(6, 3);
        a.remove(4);
        String s = "{5: 2, 6: 1}";
        assertEquals(s, a.toString());
    }

    @Test
    public void leftLeftRemove01() {
        a.insert(20, 1);
        a.insert(10, 2);
        a.insert(30, 3);
        a.insert(9, 4);
        a.insert(16, 5);
        a.insert(25, 6);
        a.insert(5, 7);
        a.remove(16);
        String s = "{5: 1, 9: 2, 10: 1, 20: 3, 25: 1, 30: 2}";
        assertEquals(s, a.toString());
    }

    @Test 
    public void leftRightRemove01() {
        a.insert(20, 1);
        a.insert(10, 2);
        a.insert(30, 3);
        a.insert(9, 4);
        a.insert(16, 5);
        a.insert(31, 6);
        a.insert(17, 7);
        a.remove(31);
        String s = "{9: 1, 10: 2, 16: 3, 17: 1, 20: 2, 30: 1}";
        assertEquals(s, a.toString());
    }

    @Test 
    public void rightRightRemove01() {
        a.insert(20, 1);
        a.insert(10, 2);
        a.insert(30, 3);
        a.insert(9, 4);
        a.insert(40, 5);
        a.insert(25, 6);
        a.insert(35, 7);
        a.remove(9);
        String s = "{10: 1, 20: 2, 25: 1, 30: 3, 35: 1, 40: 2}";
        assertEquals(s, a.toString());
    }

    @Test
    public void rightLeftRemove01() {
        a.insert(20, 1);
        a.insert(10, 2);
        a.insert(30, 3);
        a.insert(9, 4);
        a.insert(25, 5);
        a.insert(35, 6);
        a.insert(22, 7);
        a.remove(9);
        String s = "{10: 1, 20: 2, 22: 1, 25: 3, 30: 2, 35: 1}";
        assertEquals(s, a.toString());
    }
}
