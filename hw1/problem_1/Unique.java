/**
 * A class to print out all the numbers that occur in an input.
 * Author: Alice Yang
 * JHED ID: ayang36
 * HW1 - Problem 1
 * Data Structures
 */
public final class Unique {

    private Unique() {}

    /**
     * A method for seeing if a number has already occurred in the input stream.
     * @param nextInt The next integer in question
     * @param index The next empty index
     * @param inputArray The array to put unique numbers into
     * @return 0 If a match is found, 1 if not found
     */
    public static int findExisting(int nextInt, int index, int[] inputArray) {
        for (int i = 0; i < index; i++) {
            if (nextInt == inputArray[i]) {
                return 0;
            }
        }
        return 1;
    }

    /**
     * The main program.
     * @param args reads in all the input on the command line
     */
    public static void main(String[] args) {

        int arraySize = args.length;
        int nextInt = 0;
        int index = 0;
        int addNew = 1;
        int[] inputArray = new int[arraySize];

        //Scanning in the input.
        for (int i = 0; i < arraySize; i++) {

            //Throwing the IllegalArgumentException when a non-integer is read.
            try {
                Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Only integers allowed.");
            }

            nextInt = Integer.parseInt(args[i]);
            //The first element will always be unique, so it will be
            //automatically put into the array.
            if (index == 0) {
                inputArray[index] = nextInt;
                index++;
            } else {
                //Search for an existing match in the array.
                //If found, do not add to the nextInt to the array.
                addNew = findExisting(nextInt, index, inputArray);
                //Adding the nextInt in the array if it
                //doesn't exist in the array already.
                if (addNew == 1) {
                    //Putting the new unique number into the array.
                    inputArray[index] = nextInt;
                    index++;
                }
            }
        }
        //Printing out the array with uniqe numbers in it.
        for (int i = 0; i < index; i++) {
            System.out.println(inputArray[i]);
        }
    }
}
