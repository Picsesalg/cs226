/**
 * A basic counter that goes up and down by 1.
 * Author: Alice Yang
 * JHED ID: ayang36
 * HW1 - Problem 2
 * Data Structures
*/
public class BasicCounter implements ResetableCounter {

    private static final int STARTVALUE = 0;
    private int currentValue;

    /**
     * The default constructor sets the starting value to STARTVALUE.
     */
    public BasicCounter() {
        currentValue = STARTVALUE;
    }

    /**
     * Accesses the currentValue.
     * @return the currentValue
     */
    public int value() {
        return this.currentValue;
    }

    /**
     * Increments the counter up by 1.
     */
    public void up() {
        this.currentValue++;
    }

    /**
     * Decrements the counter by 1.
     */
    public void down() {
        if (this.currentValue - 1 >= 0) {
            this.currentValue--;
        }
    }

    /**
     * Resets the value to the starting value.
     */
    public void reset() {
        this.currentValue = STARTVALUE;
    }

    /**
     * The main program.
     * @param args The command line arguments
     */
    public static void main(String[] args) {

        BasicCounter basic = new BasicCounter();

        assert basic.value() == 0;
        System.out.println("Initial value & value() passed");

        basic.up();
        assert basic.value() == 1;
        System.out.println("up() passed");

        basic.up();
        basic.up();
        basic.down();
        assert basic.value() == 2;
        System.out.println("down() passed");

        basic.reset();
        assert basic.value() == 0;
        System.out.println("reset() passed");

        System.out.println("All assertions passed");
    }
}
