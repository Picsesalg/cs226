/**
 * Author: Alice Yang
 * JHED ID: ayang36
 * HW1 - Problem 2
 * Data Structures
 */
public interface ResetableCounter extends Counter {

        public void reset();

}
