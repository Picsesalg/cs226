/**
 * A counter that goes up and down by multiplying and dividing by 10.
 * Author: Alice Yang
 * JHED ID: ayang36
 * HW1 - Problem 2
 * Data Structures
 */
public class TenCounter implements ResetableCounter {

    private static final int STARTVALUE = 1;
    private int currentValue;

    /**
     * The default constructor for the counter.
     */
    public TenCounter() {
        currentValue = STARTVALUE;
    }

    /**
     * Gets the value of the counter.
     * @return The value of the counter
     */
    public int value() {
        return this.currentValue;
    }

    /**
     * Increments the counter by multiplying 10.
     */
    public void up() {
        this.currentValue *= 10;
    }

    /**
     * Decrements the counter by dividing by 10.
     */
    public void down() {
        if ((int) Math.ceil(this.currentValue / 10.0) >= 0) {
            this.currentValue = (int) Math.ceil(this.currentValue / 10.0);
        }
    }

    /**
     * Resets the counter to the starting value.
     */
    public void reset() {
        this.currentValue = STARTVALUE;
    }

    /**
     * The main program.
     * @param args The arguments on the command line
     */
    public static void main(String[] args) {

        TenCounter ten = new TenCounter();

        assert ten.value() == 1;
        System.out.println("Initial value & value() passed.");

        ten.up();
        assert ten.value() == 10;
        System.out.println("up() passed.");

        ten.up();
        ten.up();
        ten.down();
        assert ten.value() == 100;
        System.out.println("down() passed");

        ten.reset();
        assert ten.value() == 1;
        System.out.println("reset() passed.");

        System.out.println("All assertions passed.");
    }
}
