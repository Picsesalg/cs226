/**
 * A counter that starts and increments at custom values.
 * Author: Alice Yang
 * JHED ID: ayang36
 * HW1 - Problem 2
 * Data Structures
 */
public class FlexibleCounter implements ResetableCounter {

    private int startValue;
    private int currentValue;
    private int increment;

    /**
     * Default constructor for the counter.
     */
    public FlexibleCounter() {
        startValue = 0;
        currentValue = 0;
        increment = 1;
    }

    /**
     * Constructor for the counter which sets the starting and increment values.
     * @param start The starting value
     * @param incr The incrementing value
     */
    public FlexibleCounter(int start, int incr) {
        startValue = start;
        currentValue = start;
        if (incr < 0) {
            throw new IllegalArgumentException("Incremental value must be positive.");
        } else {
            increment = incr;
        }
    }

    /**
     * Gets the value of the counter.
     * @return The value of the counter
     */
    public int value() {
        return this.currentValue;
    }

    /**
     * Increments the counter by the custom increment value.
     */
    public void up() {
        this.currentValue += this.increment;
    }

    /**
     * Decrements the counter by the custom value.
     */
    public void down() {
        this.currentValue -= this.increment;
    }

    /**
     * Resets the counter to the starting value.
     */
    public void reset() {
        this.currentValue = this.startValue;
    }

    /**
     * The main program.
     * @param args The arguments in the command line
     */
    public static void main(String[] args) {

        FlexibleCounter flexible = new FlexibleCounter(20, 13);

        assert flexible.value() == 20;
        System.out.println("Initial value & value() passed.");

        flexible.up();
        assert flexible.value() == 33;
        System.out.println("up() passed.");

        flexible.up();
        flexible.up();
        flexible.down();
        assert flexible.value() == 46;
        System.out.println("down() passed.");

        flexible.reset();
        assert flexible.value() == 20;
        System.out.println("reset() passed.");

        System.out.println("All assertions passed.");
    }
}
