/**
 * A counter that increments and decrements by 2's.
 * Author: Alice Yang
 * JHED ID: ayang36
 * HW1 - Problem 2
 * Data Structures
 */
public class EvenCounter implements ResetableCounter {

    private static final int STARTVALUE = 0;
    private int currentValue;

    /**
     * Default constructor that sets the currentValue to STARTVALUE.
     */
    public EvenCounter() {
        currentValue = STARTVALUE;
    }

    /**
     * Gets the currentValue of the counter.
     * @return The currentValue of the counter
     */
    public int value() {
        return this.currentValue;
    }

    /**
     * Increments the counter by 2.
     */
    public void up() {
        this.currentValue += 2;
    }

    /**
     * Decrements the counter by 2.
     */
    public void down() {
        if (this.currentValue - 2 >= 0) {
            this.currentValue -= 2;
        }
    }

    /**
     * Resets the value of the counter to the initial value.
     */
    public void reset() {
        this.currentValue = STARTVALUE;
    }

    /**
     * The main program.
     * @param args The arguments on the command line
     */
    public static void main(String[] args) {
        EvenCounter even = new EvenCounter();

        assert even.value() == 0;
        System.out.println("Initial value & value() passed.");

        even.up();
        assert even.value() == 2;
        System.out.println("up() passed.");

        even.up();
        even.up();
        even.down();
        assert even.value() == 4;
        System.out.println("down() passed.");

        even.reset();
        assert even.value() == 0;
        System.out.println("reset() passed.");

        System.out.println("All assertions passed.");
    }
}
