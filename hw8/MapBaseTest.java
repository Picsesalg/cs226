/**
 * Alice Yang
 * ayang36@jhu.edu
 * MapBaseTest.java
 */

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import java.util.Iterator;

public abstract class MapBaseTest {

    public Map<Integer, Integer> a;

    @Before
    public abstract void setUp();

    @Test
    public void newPublicSize() {
        assertEquals(0, a.size());
    }

    @Test(expected=IllegalArgumentException.class)
    public void newPublicPut() {
        a.put(1, 1);
    }

    @Test(expected=IllegalArgumentException.class)
    public void newPublicGet() {
        a.get(1);
    }

    @Test
    public void newPublicIterator() {
        Iterator<Integer> it = a.iterator();
        int i = 0;
        while (it.hasNext()) {
            i++;
            it.next();
        }
        assertEquals(0, i);
    }

    @Test
    public void newPublicHas() {
        for(int i = 0; i < 100; i++) {
            assertEquals(false, a.has(i));
        }
    }

    @Test
    public void testInsert01() {
        a.insert(1, 1);
        assertEquals(true, a.has(1));
    }

    @Test
    public void testInsert02() {
        a.insert(1, 1);
        a.insert(12, 3);
        assertEquals(true, a.has(12));
    }

    @Test
    public void testInsert03() {
        a.insert(2, 1);
        a.insert(342, 2);
        assertEquals(false, a.has(1));
    }

    @Test
    public void testInsert04() {
        a.insert(2, 1);
        a.insert(5, 2);
        assertEquals(2, a.size());
    }

    @Test(expected=IllegalArgumentException.class)
    public void testInsertException01() {
        a.insert(null, 1);
    }

    @Test
    public void testRemove01() {
        a.insert(1, 1);
        a.remove(1);
        assertEquals(false, a.has(1));
    }

    @Test
    public void testRemove02() {
        a.insert(1, 1);
        a.insert(12, 1);
        a.insert(14, 3);
        a.remove(14);
        assertEquals(false, a.has(14));
    }

    @Test
    public void testRemove03() {
        a.insert(1, 1);
        a.insert(12, 1);
        a.insert(14, 3);
        a.remove(14);
        assertEquals(true, a.has(12));
    }

    @Test
    public void testRemove04() {
        a.insert(1, 1);
        a.insert(12, 1);
        a.insert(14, 3);
        System.out.println(a.size());
        a.remove(14);
        assertEquals(2, a.size());
    }

    @Test(expected=IllegalArgumentException.class)
    public void testRemoveException01() {
        a.insert(1, 1);
        a.remove(2);
    }

    @Test
    public void testPutGet01() {
        a.insert(1, 1);
        a.put(1, 2);
        assertEquals((Integer) 2, a.get(1));
    }

    @Test
    public void testPutGet02() {
        for (int i = 0; i < 4000; i++) {
            a.insert(i, i);
        }
        for (int i = 0; i < 4000; i++) {
            assertEquals((Integer) i, a.get(i));
        }
    }

    @Test
    public void testIterator01() {
        for (int i = 0; i < 100; i++) {
            a.insert(i, i);
        }
        for (int i = 0; i < 50; i++) {
            a.remove(i);
        }
        Iterator<Integer> it = a.iterator();
        int i = 0;
        while (it.hasNext()) {
            i++;
            it.next();
        }
        assertEquals(50, i);
    }

    @Test
    public void testGet01() {
        for (int i = 0; i < 100; i++) {
            a.insert(i, i);
        }
        for (int i = 0; i < 50; i++) {
            a.remove(i);
        }
        for (int i = 0; i < 50; i++) {
            int j = i + 50;
            assertEquals((Integer) j, a.get(j));
        }
    }

    @Test(expected=IllegalArgumentException.class)
    public void testGetException() {
        a.insert(1, 1);
        a.remove(1);
        a.get(1);
    }

    @Test
    public void test01() {
        a.insert(1, 1);
        a.insert(2, 1);
        a.remove(2);
        a.remove(1);
        assertEquals(false, a.has(1));
        assertEquals(false, a.has(2));
    }
}
