/**
 * Alice Yang
 * ayang36@jhu.edu
 * HashMapBench.java
 */

import com.github.phf.jb.Bench;
import com.github.phf.jb.Bee;

import java.lang.Math;

public class HashMapBench {

    private static final int SIZE = 100;

    private static void insertLinear(HashMap<Integer, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            m.insert(i, i);
        }
    }

    private static void insertRandom(HashMap<Integer, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            int a = (int) Math.random() * SIZE;
            if (!m.has(a)) {
                m.insert(a, i);
            }
        }
    }

    private static void removeLinear(HashMap<Integer, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            if (m.has(i)) {
                m.remove(i);
            }
        }
    }
    
    private static void removeRandom(HashMap<Integer, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            int a = (int) Math.random() * SIZE;
            if (m.has(a)) {
                m.remove(a);
            }
        }
    }

    private static void lookupLinear(HashMap<Integer, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            boolean b = m.has(i);
        }
    }

    private static void lookupRandom(HashMap<Integer, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            boolean b = m.has((int) Math.random() * SIZE);
        }
    }


    //Actual benchmarks to test now

    @Bench
    public void insertLinearHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            HashMap<Integer, Integer> m = new HashMap<Integer, Integer>();
            b.start();
            insertLinear(m);
        }
    }

    @Bench
    public void insertRandomHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            HashMap<Integer, Integer> m = new HashMap<Integer, Integer>();
            b.start();
            insertRandom(m);
        }
    }

    @Bench
    public void insertLinearRemoveLinearHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            HashMap<Integer, Integer> m = new HashMap<Integer, Integer>();
            insertLinear(m);
            b.start();
            removeLinear(m);
        }
    }

    @Bench
    public void insertLinearRemoveRandomHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            HashMap<Integer, Integer> m = new HashMap<Integer, Integer>();
            insertLinear(m);
            b.start();
            removeRandom(m);
        }
    }

    @Bench
    public void insertRandomRemoveLinearHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            HashMap<Integer, Integer> m = new HashMap<Integer, Integer>();
            insertRandom(m);
            b.start();
            removeLinear(m);
        }
    }

    @Bench
    public void insertRandomRemoveRandomHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            HashMap<Integer, Integer> m = new HashMap<Integer, Integer>();
            insertRandom(m);
            b.start();
            removeRandom(m);
        }
    }

    @Bench
    public void insertLinearLookUpLinearHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            HashMap<Integer, Integer> m = new HashMap<Integer, Integer>();
            insertLinear(m);
            b.start();
            lookupLinear(m);
        }
    }

    @Bench
    public void insertLinearLookUpRandomHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            HashMap<Integer, Integer> m = new HashMap<Integer, Integer>();
            insertLinear(m);
            b.start();
            lookupRandom(m);
        }
    }

    @Bench
    public void insertRandomLookUpLinearHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            HashMap<Integer, Integer> m = new HashMap<Integer, Integer>();
            insertRandom(m);
            b.start();
            lookupLinear(m);
        }
    }

    @Bench
    public void insertRandomLookUpRandomHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            HashMap<Integer, Integer> m = new HashMap<Integer, Integer>();
            insertRandom(m);
            b.start();
            lookupRandom(m);
        }
    }
}
