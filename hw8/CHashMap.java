/**
 * ALice Yang
 * ayang36@jhu.edu
 * HashMap.java
 */

import java.util.ArrayList;
import java.util.Iterator;

public class HashMap<K, V> implements Map<K, V> {

    private class Entry {
        K key;
        V value;
        Entry next;

        Entry(K k, V v) {
            this.key = k;
            this.value = v;
            this.next = null;
        }
    }

    private SimpleArray<Entry> data;
    private int capacity;
    private int size;

    public HashMap() {
        this.capacity = 103;
        this.data = new SimpleArray<Entry>(this.capacity, null);
        this.size = 0;
    }

    /*private Entry find(K k) {
        int hash = k.hashCode() % this.capacity;
        Entry e = this.data.get(hash);
        Entry prev = null;
        while (e != null) {
            if (e.key.equals(k)) {
                return prev;
            }
            prev = e;
            e = e.next;
        }
        return null;
    }

    private Entry findForSure(K k) {
        Entry e = this.find(k);
        if (e == null) {
            throw new IllegalArgumentException();
        }
        return e;
    }*/

    private void expand() {
        SimpleArray<Entry> temp = new SimpleArray<Entry>(this.capacity, null);
        for (int i = 0; i < this.capacity; i++) {
            temp.put(i, this.data.get(i));
        }

        this.size = 0;
        this.capacity *= 2;
        this.data = new SimpleArray<Entry>(this.capacity, null);

        for (int i = 0; i < this.capacity / 2; i++) {
            Entry e = temp.get(i);
            while (e != null) {
                this.insert(e.key, e.value);
                e = e.next;
            }
        }
    }

    @Override
    public void insert(K k, V v) throws IllegalArgumentException {
        if (k == null) {
            throw new IllegalArgumentException();
        }

        /*if (e != null) {
            throw new IllegalArgumentException();
        }

        e = new Entry(k, v);
        int index = k.hashCode() % this.capacity;
        e.next = this.data.get(index);
        this.data.put(index, e);
        this.size++;

        if (1.0 * this.size / this.capacity > 0.7) {
            this.expand();
        }*/

        int i = (k.hashCode() & 0x7fffffff) % this.capacity;
        Entry e = this.data.get(i);
        while (e != null) {
            if (e.key.equals(k)) {
                throw new IllegalArgumentException("Duplicate key");
            }
            e = e.next;
        }
        Entry entry = new Entry(k, v);
        entry.next = this.data.get(i);
        this.data.put(i, entry);
        this.size++;

        if (1.0 * this.size / this.capacity > 0.7) {
            this.expand();
        }
    }

    @Override
    public V remove(K k) throws IllegalArgumentException {

        if (k == null) {
            throw new IllegalArgumentException();
        }

        /*Entry prev = this.findForSure(k);
        Entry e = prev.next;
        prev.next = e.next;

        this.size--;

        return e.value;*/

        int i = (k.hashCode() & 0x7fffffff) % this.capacity;
        Entry e = this.data.get(i);
        Entry prev = null;

        while (e != null) {
            if (e.key.equals(k)) {
                if (prev != null) {
                    prev.next = e.next;
                } else {
                    this.data.put(i, e.next);
                }
                return e.value;
            }
            e = e.next;
        }

        this.size--;

        throw new IllegalArgumentException("Key not in map");
    }

    @Override
    public void put(K k, V v) {
        if (k == null) {
            throw new IllegalArgumentException();
        }

        int i = (k.hashCode() & 0x7fffffff) % this.capacity;
        Entry e = this.data.get(i);
        boolean flag = true;

        while (e != null) {
            if (e.key.equals(k)) {
                e.value = v;
                flag = false;
                break;
            }
            e = e.next;
        }
        if (flag) {
            throw new IllegalArgumentException();
        }
    }

    @Override 
    public V get(K k) throws IllegalArgumentException {
        if (k == null) {
            throw new IllegalArgumentException();
        }

        /*Entry prev = this.findForSure(k);
        return prev.next.value;*/

        int i = (k.hashCode() & 0x7fffffff) % this.capacity;
        Entry e = this.data.get(i);
        while (e != null) {
            if (e.key.equals(k)) {
                return e.value;
            }
            e = e.next;
        }

        throw new IllegalArgumentException("Key not in");
    }

    @Override
    public boolean has(K k) {
        int i = (k.hashCode() & 0x7fffffff) % this.capacity;
        Entry e = this.data.get(i);
        while (e != null) {
            if (e.key.equals(k)) {
                return true;
            }
            e = e.next;
        }
        return false;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public Iterator<K> iterator() {
        ArrayList<K> keys = new ArrayList<K>();
        for (int i = 0; i < this.capacity; i++) {
            Entry e = this.data.get(i);
            while (e != null) {
                keys.add(e.key);
                e = e.next;
            }
        }
        return keys.iterator();
    }

    @Override
    public String toString() {
        String s = "{\n";
        for (int i = 0; i < this.capacity; i++) {
            Entry e = this.data.get(i);
            while (e != null) {
                s += e.key;
                s += e.value;
                s += "\n";
            }
        }
        s += "}";
        return s;
    }
}
