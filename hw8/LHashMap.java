/**
 * Alice Yang
 * ayang36@jhu.edu
 * HashMap.java
 */

import java.util.Iterator;
import java.util.ArrayList;

/**
 * Hash Map implementation using linear probing.
 * @param <K> Key type
 * @param <V> Value type
 */
public class HashMap<K, V> implements Map<K, V> {

    private SimpleArray<K> keys;
    private SimpleArray<V> values;
    private int capacity;
    private int size;

    /**
     * Default constructor.
     */
    public HashMap() {
        this.keys = new SimpleArray<K>(103, null);
        this.values = new SimpleArray<V>(103, null);
        this.capacity = 103;
        this.size = 0;
    }

    private int find(K k) {
        if (k == null) {
            throw new IllegalArgumentException("Key can't be null");
        }

        int hash = k.hashCode() % this.capacity;
        int i = hash;

        do {
            if (k.equals(this.keys.get(i))) {
                return i;
            }
            i = (i + 1) % this.capacity;
        } while (i != hash);

        return -1;
    }

    private void expand() {
        SimpleArray<K> tempK = new SimpleArray<K>(this.capacity, null);
        SimpleArray<V> tempV = new SimpleArray<V>(this.capacity, null);

        for (int i = 0; i < this.capacity; i++) {
            tempK.put(i, this.keys.get(i));
            tempV.put(i, this.values.get(i));
        }

        this.keys = new SimpleArray<K>(this.capacity * 2, null);
        this.values = new SimpleArray<V>(this.capacity * 2, null);
        this.size = 0;
        this.capacity *= 2;

        for (int i = 0; i < this.capacity / 2; i++) {
            this.insert(tempK.get(i), tempV.get(i));
        }
    }

    @Override
    public void insert(K k, V v) {
        
        int key_pos = this.find(k);

        if (key_pos != -1) {
            throw new IllegalArgumentException("Duplicate key.");
        }

        int hash = k.hashCode() % this.capacity;
        int i = hash;

        do {
            if (this.keys.get(i) == null) {
                this.keys.put(i, k);
                this.values.put(i, v);
                break;
            }
            i = (i + 1) % this.capacity;
        } while (i != hash);

        this.size++;

        if (this.size / this.capacity > 0.5) {
            this.expand();
        }
    }

    @Override
    public V remove(K k) {

        int key_pos = this.find(k);

        if (key_pos == -1) {
            throw new IllegalArgumentException("Key doesn't exist");
        }

        this.keys.put(key_pos, null);
        V v = this.values.get(key_pos);
        this.values.put(key_pos, null);
        this.size--;
        return v;
    }

    @Override
    public void put(K k, V v) {

        int key_pos = this.find(k);

        if (key_pos == -1) {
            throw new IllegalArgumentException("Key not in map");
        }

        this.values.put(key_pos, v);
    }

    @Override
    public V get(K k) {

        int key_pos = this.find(k);

        if (key_pos == -1) {
            throw new IllegalArgumentException("Key not in map.");
        }

        return this.values.get(key_pos);
    }

    @Override
    public boolean has(K k) {
        return this.find(k) != -1;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public Iterator<K> iterator() {

        ArrayList<K> it = new ArrayList<K>();

        for (int i = 0; i < this.capacity; i++) {
            K k = this.keys.get(i);
            if (k != null) {
                it.add(k);
            }
        }

        return it.iterator();
    }

    @Override
    public String toString() {
        String s = "{";
        for (int i = 0; i < this.capacity; i++) {
            K k = this.keys.get(i);
            if (k != null) {
                s += "" + k + ": " + this.values.get(i);
                if (i < this.capacity - 1) {
                    s += ",\n";
                }
            }
        }
        s += "}";
        return s;
    }
}
