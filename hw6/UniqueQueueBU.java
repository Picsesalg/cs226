/**
 * Alice Yang
 * ayang36@jhu.edu
 * UniqueQueueBU.java
 */

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Driver program for bottom up construction of binary heap
 * for the Unique program.
 */
public final class UniqueQueueBU {

    private static BinaryHeapPriorityQueue<Integer> data;

    private UniqueQueueBU() {}

    /**
     * Main program.
     * @param args Arguments from command line
     */
    public static void main(String[] args) {

        ArrayList<Integer> temp = new ArrayList<Integer>();

        Scanner kb = new Scanner(System.in);

        while (kb.hasNextInt()) {
            int i = kb.nextInt();
            temp.add(i);
        }

        data = new BinaryHeapPriorityQueue<Integer>();

        for (int i = 0; i < temp.size(); i++) {
            data.bottomUp(temp);
        }

        Integer prev = null;

        while (!data.empty()) {
            Integer i = data.top();
            if (prev == null || i != prev) {
                System.out.println(i);
            }
            prev = i;
            data.remove();
        }
    }
}
