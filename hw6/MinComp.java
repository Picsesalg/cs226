import java.util.Comparator;

public class MinComp<T extends Comparable<? super T>>
    implements Comparator<T> {
        public int compare(T t1, T t2) {
            return t2.compareTo(t1);
        }
}
