import java.util.Comparator;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.assertEquals;

public class Test1 {

    private BinaryHeapPriorityQueue<String> a;

    @Before
    public void setUp() {
        a = new BinaryHeapPriorityQueue<String>(new MinComp<String>());
    }

    @Test
    public void test1() {
        a.insert("1");
        a.insert("3");
        a.insert("2");
        String s  = "";
        s += a.top();
        a.remove();
        s += a.top();
        a.remove();
        s += a.top();
        assertEquals("123", s);
    }
}
