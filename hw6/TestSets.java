/**
 * Alice Yang
 * ayang36@jhu.edu
 * TestSets.java
 */

import java.util.Iterator;
import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.assertEquals;

public class TestSets {

    MoveToFrontListSet<String> list;
    TransposeArraySet<String> array;

    @Before
    public void setUpSets() {
        list = new MoveToFrontListSet<String>();
        array = new TransposeArraySet<String>();
    }

    @Test
    public void newListInsert() {
        list.insert("One");
        Iterator<String> it = list.iterator();
        assertEquals("One", it.next());
    }

    @Test 
    public void testRepeatedListInsert() {
        list.insert("One");
        list.insert("One");
        int i = 0;
        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            i++;
            it.next();
        }
        assertEquals(1, i);
    }
    
    @Test 
    public void newRemove() {
        list.insert("One");
        list.insert("Two");
        list.remove("4");
        Iterator<String> it = list.iterator();
        int i = 0;
        while (it.hasNext()) {
            i++;
            it.next();
        }
        assertEquals(2, i);
    }

    @Test
    public void testListRemove() {
        list.insert("One");
        list.insert("Two");
        list.remove("One");
        int i = 0;
        Iterator<String> it = list.iterator();
        while(it.hasNext()) {
            i++;
        }
        assertEquals(1, i);
    }

    @Test
    public void newListHas() {
        list.insert("One");
        list.insert("Two");
        list.has("One");
        Iterator<String> it = list.iterator();
        assertEquals("One", it.next());
    }

    @Test
    public void newArrayInsert() {
        array.insert("one");
        Iterator<String> it = array.iterator();
        assertEquals("one", it.next());
    }

    @Test
    public void newArrayRemove() {
        array.insert("VR");
        array.insert("BT");
        array.remove("VR");
        Iterator<String> it = array.iterator();
        int i = 0;
        while (it.hasNext()) {
            i++;
            it.next();
        }
        assertEquals(1, i);
    }

    @Test
    public void newArrayHas() {
        array.insert("one");
        array.insert("two");
        array.has("one");
        Iterator<String> it = array.iterator();
        assertEquals("one", it.next());
    }
}
