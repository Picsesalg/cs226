/**
 * Alice Yang
 * ayang36@jhu.edu
 * UniqueQueue.java
 */

import java.util.Scanner;

/**
 * Program to run the old Unique program using BinaryHeap.
 */
public final class UniqueQueue {

    private static PriorityQueue<Integer> queue;

    private UniqueQueue() {}

    /**
     * Main driver program.
     * @param args Command line arguments
     */
    public static void main(String[] args) {

        queue = new SortedArrayPriorityQueue<Integer>();

        Scanner kb = new Scanner(System.in);

        while (kb.hasNextInt()) {
            int i = kb.nextInt();
            queue.insert(i);
        }

        Integer prev = null;

        while (!queue.empty()) {
            Integer i = queue.top();
            if (prev == null || i != prev) {
                System.out.println(i);
            }
            prev = i;
            queue.remove();
        }
    }
}
