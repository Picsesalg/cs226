/**
 * Alice Yang
 * ayang36@jhu.edu
 * BinaryHeapPriorityQueue.java
 */

import java.util.ArrayList;
import java.util.Comparator;

/**
 * A class that will implement a binary heap which will
 * be used as a priority queue.
 * @param <T> Generic variable type
 */
public class BinaryHeapPriorityQueue<T extends Comparable<? super T>>
    implements PriorityQueue<T> {

    private static class DefaultComparator<T extends Comparable<? super T>>
        implements Comparator<T> {
        public int compare(T t1, T t2) {
            return t1.compareTo(t2);
        }
    }

    private ArrayList<T> queue;
    private Comparator<T> cmp;

    /**
     * Default constructor that uses the default comparator.
     */
    public BinaryHeapPriorityQueue() {
        this.queue = new ArrayList<T>();
        this.cmp = new DefaultComparator<T>();
    }

    /**
     * Parameterised constructor with a custom comparator.
     * @param cmp The user's own comparator
     */
    public BinaryHeapPriorityQueue(Comparator<T> cmp) {
        this.queue = new ArrayList<T>();
        this.queue.add(null);
        this.cmp = cmp;
    }

    private boolean gt(int i, int j) {
        if (i == -1 || j == -1) {
            return false;
        }
        return this.cmp.compare(this.queue.get(i), this.queue.get(j)) > 0;
    }

    private int parent(int i) {
        return (i - 1) / 2;
    }

    private int left(int i) {
        return i * 2 + 1;
    }

    private int right(int i) {
        return i * 2 + 2;
    }

    /**
     * FInds the largest child of a node.
     * @param i THe node in question
     * @return -1 if no children, left index if left, right index if right
     */
    private int largestChild(int i) {
        if (i * 2 + 1 < this.queue.size()) {
            if (i * 2 + 2 < this.queue.size()) {
                if (this.gt(left(i), right(i))) {
                    return left(i);
                } else {
                    return right(i);
                }
            } else {
                return left(i);
            }
        } else {
            return -1;
        }
    }

    @Override
    public void insert(T t) {

        this.queue.add(t);

        int old = this.queue.size() - 1;

        /* Keeps swapping until ordering property isn't violated. */
        while (old != 0 && this.gt(old, parent(old))) {
            this.queue.set(old, this.queue.get(parent(old)));
            this.queue.set(parent(old), t);
            old = parent(old);
        }
    }

    @Override
    public void remove() throws EmptyException {

        if (this.queue.isEmpty()) {
            throw new EmptyException();
        }

        /* Moving the bottom number up, and deleting. */
        this.queue.set(0, this.queue.get(this.queue.size() - 1));
        this.queue.remove(this.queue.size() - 1);

        int old = 0;

        /* Swap down */
        while (old < this.queue.size() && this.gt(largestChild(old), old)) {
            T swapping = this.queue.get(old);
            int j = largestChild(old);
            this.queue.set(old, this.queue.get(j));
            this.queue.set(largestChild(old), swapping);
            old = j;
        }
    }

    @Override
    public T top() throws EmptyException {

        if (this.queue.isEmpty()) {
            throw new EmptyException();
        }

        return this.queue.get(0);
    }

    @Override
    public boolean empty() {
        return this.queue.isEmpty();
    }

    /**
     * Bottom up construction of binary heap starting with
     * a randomly inserted array.
     * @param ra The array list to be converted to a binary heap
     */
    public void bottomUp(ArrayList<T> ra) {

        this.queue = ra;

        if (ra.isEmpty()) {
            return;
        }

        for (int i = parent(this.queue.size() - 1); i >= 0; i--) {
            int old = i;
            while (largestChild(old) != -1 && this.gt(largestChild(old), old)) {
                T parent = this.queue.get(old);
                int j = largestChild(old);
                this.queue.set(old, this.queue.get(largestChild(old)));
                this.queue.set(largestChild(old), parent);
                old = j;
            }
        }
    }
}
