/**
 * Alice Yang
 * ayang36@jhu.edu
 * TestQueue.java
 */

import java.util.ArrayList;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.assertEquals;

public class TestQueue {

    private BinaryHeapPriorityQueue<String> a;

    @Before
    public void setUp() {
        a = new BinaryHeapPriorityQueue<String>();
    }

    @Test
    public void testInset01() {
        String s = "";
        a.insert("1");
        s += a.top();
        assertEquals("1", s);
    }

    @Test
    public void testInsert02() {
        a.insert("a");
        a.insert("r");
        a.insert("A");
        assertEquals("r", a.top());
    }

    @Test
    public void testInsert03() {
        a.insert("Z");
        a.insert("Y");
        a.insert("y");
        assertEquals("y", a.top());
    }

    @Test
    public void testInsert04() {
        a.insert("a");
        a.insert("b");
        a.insert("c");
        a.insert("b");
        assertEquals("c", a.top());
    }

    @Test
    public void testInsertRemove01() {
        String s = "";
        a.insert("a");
        a.insert("b");
        a.insert("G");
        s += a.top();
        a.remove();
        s += a.top();
        a.remove();
        s += a.top();
        a.remove();
        assertEquals("baG", s);
    }

    @Test
    public void testInsertRemove02() {
        String s = "";
        a.insert("a");
        a.insert("b");
        a.insert("c");
        a.insert("b");
        s += a.top();
        a.remove();
        s += a.top();
        a.remove();
        s += a.top();
        a.remove();
        s += a.top();
        assertEquals("cbba", s);
    }

    @Test(expected=EmptyException.class)
    public void newRemoveException01() {
        a.remove();
    }

    @Test(expected=EmptyException.class)
    public void removeException01() {
        a.insert("f");
        a.remove();
        a.remove();
    }

    @Test(expected=EmptyException.class)
    public void newTopException01() {
        a.top();
    }

    @Test
    public void newBottomUp() {
        ArrayList<String> ra = new ArrayList<String>();
        ra.add("2");
        ra.add("9");
        ra.add("7");
        ra.add("6");
        ra.add("5");
        ra.add("8");
        a.bottomUp(ra);
        String s = "";
        s += a.top();
        a.remove();
        s += a.top();
        a.remove();
        s += a.top();
        a.remove();
        s += a.top();
        a.remove();
        s += a.top();
        a.remove();
        s += a.top();
        a.remove();
        assertEquals("987652", s);
    }
}
