#!/bin/sh

## Compile non-benchmark code
##for i in `/bin/ls *.java | grep -v Bench`
##do
##  echo "Compile $i"
##  javac $i
##done

## Compile with jaybee
javac -cp .:../../datastructures2018/resources/jaybee-1.0.jar PriorityQueuesBench.java

## Now run with jaybee
java -jar ../../datastructures2018/resources/jaybee-1.0.jar PriorityQueuesBench
