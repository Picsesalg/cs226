import com.github.phf.jb.Bench;
import com.github.phf.jb.Bee;

import java.util.Random;

/**
 * Compare performance of SortedArrayPriorityQueue and BinaryHeapPriorityQueue.
 */
public final class PriorityQueuesBench {
    private static final int SIZE = 100;
    private static final Random RAND = new Random();

    // First some basic "compound operations" to benchmark. Note that each
    // of these is carefully dimensioned (regarding the range of elements)
    // to allow combining them.

    // Insert a number of "consecutive" strings into the given set.
    private static void insertLinear(PriorityQueue<String> s) {
        for (int i = 0; i < SIZE; i++) {
            s.insert(Integer.toString(i));
        }
    }

    // Insert a number of "random" strings into the given set.
    private static void insertRandom(PriorityQueue<String> s) {
        for (int i = 0; i < SIZE; i++) {
            s.insert(Integer.toString(RAND.nextInt(SIZE * 4)));
        }
    }

    // Remove a number of "random" strings from the given set.
    private static void remove(PriorityQueue<String> s) {
        for (int i = 0; i < SIZE; i++) {
            s.remove();
        }
    }

    // Now the benchmarks we actually want to run.

    @Bench
    public void insertLinearSortedArrayPriorityQueue(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            PriorityQueue<String> s = new SortedArrayPriorityQueue<>();
            b.start();
            insertLinear(s);
        }
    }

    @Bench
    public void insertLinearBinaryHeapPriorityQueue(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            PriorityQueue<String> s = new BinaryHeapPriorityQueue<>();
            b.start();
            insertLinear(s);
        }
    }

    @Bench
    public void insertRandomSortedArrayPriorityQueue(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            PriorityQueue<String> s = new SortedArrayPriorityQueue<>();
            b.start();
            insertRandom(s);
        }
    }

    @Bench
    public void insertRandomBinaryHeapPriorityQueue(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            PriorityQueue<String> s = new BinaryHeapPriorityQueue<>();
            b.start();
            insertRandom(s);
        }
    }

    @Bench
    public void removeSortedArrayPriorityQueue(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            PriorityQueue<String> s = new SortedArrayPriorityQueue<>();
            insertRandom(s);
            b.start();
            remove(s);
        }
    }

    @Bench
    public void removeBinaryHeapPriorityQueue(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            PriorityQueue<String> s = new BinaryHeapPriorityQueue<>();
            insertRandom(s);
            b.start();
            remove(s);
        }
    }
}
