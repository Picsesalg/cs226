/**
 * ALice Yang
 * ayang36@jhu.edu
 * RLEDecoder.java
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/**
  Computes a Run Length Encoding of a string.
**/
public final class RLEDecoder {

    /** Make checkstyle happy. */
    private RLEDecoder() { }

    /** Run Length Decode the String.

        @param input The input RLE String
        @return The decoded RLE string
        @throws IOException On invalid input
    */
    public static String runLengthDecode(String input) throws IOException {

        if (input == null) {
            throw new IOException();
        }

        if (input.length() == 0) {
            return "";
        }

        if (Character.isDigit(input.charAt(0))) {
            throw new IOException();
        }

        StringBuilder out = new StringBuilder();
        String num = "";
        char cur = 'a';
        int i = 0;

        while (i < input.length()) {
            if (!Character.isDigit(input.charAt(i))) {
                cur = input.charAt(i);
                out.append(cur);
                i++;
                continue;
            }
            do {
                num += input.charAt(i);
                i++;
            } while (i < input.length() && Character.isDigit(input.charAt(i)));
            for (int j = 1; j < Integer.parseInt(num); j++) {
                out.append(cur);
            }
            num = "";
        }

        return out.toString();
    }

    /** Reads the input from standard in.
        @return Returns a string with the input data without newlines
        @throws IOException if the file cannot be read
    */
    public static String readInput() throws IOException {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(input);
        StringBuilder sb = new StringBuilder();

        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();
        input.close();

        return sb.toString();
    }

    /** Main method, load file into string, compute BWT.
        @param args Input arguments, ingorned
    */
    public static void main(String[] args) {
        String text = "";

        try {
            text = readInput();
        } catch (IOException e) {
            System.err.println("Cant read input");
            System.exit(1);
        }

        try {
            System.out.println(runLengthDecode(text));
        } catch (IOException e) {
            System.err.println("Invalid Input");
            System.exit(1);
        }
    }
}
