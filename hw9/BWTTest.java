/**
 * Alice Yang
 * ayang36@jhu.edu
 * BWTTest.java
 */

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import java.io.IOException;

public class BWTTest {

    private BWTEncoder e;
    private BWTDecoder d;
    private String a;
    private String b;

    @Before
    public void setUp() {
        e = new BWTEncoder();
    }

    @Test
    public void testEncoder01() {
        String a = "a";

        try {
            b = e.createBWT(a);
        } catch (IOException e) {
            b = "Whoops";
        }
        assertEquals("a$", b);
    }

    @Test
    public void testEncoder02() {
        String a = "aaaw";

        try {
            b = e.createBWT(a);
        } catch (IOException e) {
            b = "Whoops";
        }
        assertEquals("w$aaa", b);
    }

    @Test
    public void testEncode03() {
        String a = "abbcfd";

        try {
            b = e.createBWT(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals("d$abbfc", b);
    }

    @Test
    public void testEncode04() {
        String a = "Wollongong";

        try {
            b = e.createBWT(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals("g$nnolooWgl", b);
    }

    @Test
    public void testEncode05() {
        String a = "Warrawong";

        try {
            b = e.createBWT(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals("g$Wrnowraa", b);
    }

    @Test
    public void testEncodeException01() {
        String a = null;

        try {
            b = e.createBWT(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals("Whoops", b);
    }

    @Test
    public void testDecoder01() {
        String a = "a$";

        try {
            b = d.decodeBWT(a);
        } catch (IOException e) {
            b = "Whoops";
        }
        assertEquals("a", b);
    }

    @Test
    public void testDecoder02() {
        String a = "w$aaa";

        try {
            b = d.decodeBWT(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals("aaaw", b);
    }

    @Test
    public void testDecoder03() {
        String a = "d$abbfc";

        try {
            b = d.decodeBWT(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals("abbcfd", b);
    }

    @Test
    public void testDecoder04() {
        String a = "g$nnolooWgl";

        try {
            b = d.decodeBWT(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals("Wollongong", b);
    }

    @Test
    public void testDecoder05() {
        String a = "g$Wrnowraa";

        try {
            b = d.decodeBWT(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals("Warrawong", b);
    }

    @Test
    public void testDecodeException01() {
        String a = null;

        try {
            b = d.decodeBWT(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals("Whoops", b);
    }

    @Test
    public void testEncodeDecode01() {
        String a = "a";

        try {
            b = e.createBWT(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        try {
            b = d.decodeBWT(b);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals(a, b);
    }

    @Test
    public void testEncodeDecode02() {
        String a = "aaabbbbcccccdddddd";

        try {
            b = e.createBWT(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        try {
            b = d.decodeBWT(b);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals(a, b);
    }
}
