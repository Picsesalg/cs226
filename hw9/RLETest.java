/**
 * Alice Yang
 * ayang36@jhu.edu
 * RLETest.java
 */

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import java.io.IOException;

public class RLETest {

    private RLEEncoder e;
    private RLEDecoder d;
    private String b;

    @Before
    public void setUp() {}

    @Test
    public void testEncoder01() {
        String a = "a";

        try {
            b = e.runLengthEncode(a);
        } catch (IOException e) {
            b = "Whoops";
        }
        assertEquals("a", b);
    }

    @Test
    public void testEncoder02() {
        String a = "aaaw";

        try {
            b = e.runLengthEncode(a);
        } catch (IOException e) {
            b = "Whoops";
        }
        assertEquals("a3w", b);
    }

    @Test
    public void testEncode03() {
        String a = "abbbbbbcfd";

        try {
            b = e.runLengthEncode(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals("ab6cfd", b);
    }

    @Test
    public void testEncode04() {
        String a = "abccccc";

        try {
            b = e.runLengthEncode(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals("abc5", b);
    }

    @Test
    public void testEncode05() {
        String a = "aaabbbbcccccdddddd";

        try {
            b = e.runLengthEncode(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals("a3b4c5d6", b);
    }

    @Test
    public void testEncode07() {
        String a = "";

        try {
            b = e.runLengthEncode(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals("", b);
    }

    @Test
    public void testEncode06() {
        String a = "a b  ";

        try {
            b = e.runLengthEncode(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals("a b 2", b);
    }

    @Test
    public void testEncoderException01() {
        String a = null;

        try {
            b = e.runLengthEncode(a);
        } catch (IOException e) {
            b = "Whoops";
        }
        assertEquals("Whoops", b);
    }

    @Test
    public void testDecoder01() {
        String a = "a";

        try {
            b = d.runLengthDecode(a);
        } catch (IOException e) {
            b = "Whoops";
        }
        assertEquals("a", b);
    }

    @Test
    public void testDecoder02() {
        String a = "a3w";

        try {
            b = d.runLengthDecode(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals("aaaw", b);
    }

    @Test
    public void testDecoder03() {
        String a = "ab6cf";

        try {
            b = d.runLengthDecode(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals("abbbbbbcf", b);
    }

    @Test
    public void testDecoder04() {
        String a = "abc5";

        try {
            b = d.runLengthDecode(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals("abccccc", b);
    }

    @Test
    public void testDecoder05() {
        String a = "a3b4c5d6";

        try {
            b = d.runLengthDecode(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals("aaabbbbcccccdddddd", b);
    }

    @Test
    public void testDecoder06() {
        String a = "";

        try {
            b = d.runLengthDecode(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals("", b);
    }

    @Test
    public void testDecodeException01() {
        String a = null;

        try {
            b = d.runLengthDecode(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals("Whoops", b);
    }

    @Test
    public void testDecodeException02() {
        String a = "12a b2";

        try {
            b = d.runLengthDecode(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals("Whoops", b);
    }

    @Test
    public void testEncodeDecode01() {
        String a = "a";

        try {
            b = e.runLengthEncode(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        try {
            b = d.runLengthDecode(b);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals(a, b);
    }

    @Test
    public void testEncodeDecode02() {
        String a = "aaabbbbcccccdddddd";

        try {
            b = e.runLengthEncode(a);
        } catch (IOException e) {
            b = "Whoops";
        }

        try {
            b = d.runLengthDecode(b);
        } catch (IOException e) {
            b = "Whoops";
        }

        assertEquals(a, b);
    }
}
