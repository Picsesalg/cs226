/**
 * Alice Yang
 * ayang36@jhu.edu
 * BWTDecoder.java
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Arrays;

/** Class to decode a BWT back to the original string. */
public final class BWTDecoder {

    /** Make checkstyle happy. */
    private BWTDecoder() { }

    /** Decode the BWT of a string.
        @param bwt the input bwt string
        @return The original string (before BWT)
        @throws IOException On invalid input
    */
    public static String decodeBWT(String bwt) throws IOException {

        if (bwt == null) {
            throw new IOException();
        }

        StringBuilder text = new StringBuilder();

        char[] firstCol = bwt.toCharArray();
        Arrays.sort(firstCol);

        int[] bwtIterations = new int[bwt.length()];

        for (int i = 0; i < bwt.length(); i++) {
            int counter = 1;
            char cur = bwt.charAt(i);
            for (int j = i - 1; j >= 0; j--) {
                if (bwt.charAt(j) == cur) {
                    counter++;
                }
            }
            bwtIterations[i] = counter;
        }

        int i = 0;
        while (bwt.charAt(i) != '$') {
            text.append(bwt.charAt(i));
            i = applyLF(firstCol, bwt.charAt(i), bwtIterations[i]);
        }

        return text.reverse().toString();
    }

    private static int applyLF(char[] firstCol, char c, int it) {
        int counter = 0;
        for (int i = 0; i < firstCol.length; i++) {
            if (firstCol[i] == c) {
                counter++;
                if (counter == it) {
                    return i;
                }
            }
        }
        return -1;
    }

    /** Reads the input data into a string.
        @return A string with the input data wiht newlines removed
        @throws IOException On error reading input
    */
    public static String readInput() throws IOException {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(input);
        StringBuilder sb = new StringBuilder();

        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();
        input.close();

        return sb.toString();
    }

    /** Reads a BWT string from standard in and returns the original string.
        @param args ignored
    */
    public static void main(String[] args) {
        String bwt = "";

        try {
            bwt = readInput();
        } catch (IOException e) {
            System.err.println("Cant read input");
            System.exit(1);
        }

        try {
            System.out.println(BWTDecoder.decodeBWT(bwt));
        } catch (IOException e) {
            System.err.println("Invalid input");
            System.exit(1);
        }
    }
}
