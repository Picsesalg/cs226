/**
 * Alice Yang
 * ayang36@jhu.edu
 * BWTEncoder.java
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/**
  Builds the BWT of a string.

  Builds the BWT by sorting all of the cyclic permutations.
**/
public class BWTEncoder {

    String text;

    /** Silence checkstyle. */
    public BWTEncoder() { }

    /** Construct the BWT of a string.

        Derives the list of possible permutations, sorts them, and then
        extracts the last colunm

        @param input The input String
        @return The BWT of the input string
        @throws IOException On invalid input
    */
    public String createBWT(String input) throws IOException {

        if (input == null) {
            throw new IOException();
        }

        this.text = input + "$";

        String lastCol = this.createPermutations(text);

        return lastCol;
    }

    private String createPermutations(String s) {
        int size = s.length();
        StringBuilder lastCol = new StringBuilder();
        int[] offsets = new int[size];

        for (int i = 0; i < size; i++) {
            offsets[i] = i;
        }

        quickSort(offsets, 0, size - 1, s);

        for (int i = 0; i < size; i++) {
            int index = (offsets[i] - 1) % size;
            if (index < 0) {
                index += size;
            }
            lastCol.append(s.charAt(index));
        }

        return lastCol.toString();
    }

    private void quickSort(int[] ra, int start, int end, String s) {
        if (start < end) {
            int partIndex = partition(ra, start, end, s);

            quickSort(ra, start, partIndex - 1, s);
            quickSort(ra, partIndex + 1, end, s);
        }
    }

    private static int partition(int[] ra, int start, int end, String s) {
        //CompareSuffixes cmp = new CompareSuffixes();
        int pivot = ra[end];
        int j = start - 1;

        for (int i = start; i < end; i++) {
            //if (ra[i] < pivot) {
            if (CompareSuffixes.cmpSuffixes(s, ra[i], pivot) == -1) {
                j++;
                int temp = ra[j];
                ra[j] = ra[i];
                ra[i] = temp;
            }
        }

        int temp = ra[j + 1];
        ra[j + 1] = ra[end];
        ra[end] = temp;

        return j + 1;
    }

    /** Reads the input from standard in.
        @return Returns a string with the input data without newlines
        @throws IOException if the file cannot be read
    */
    public static String readInput() throws IOException {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(input);
        StringBuilder sb = new StringBuilder();

        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();
        input.close();

        return sb.toString();
    }

    /** Main method, load file into string, compute BWT.
        @param args Input arguments, ingorned
    */
    public static void main(String[] args) {
        String text = "";

        try {
            text = readInput();
        } catch (IOException e) {
            System.err.println("Cant read input");
            System.exit(1);
        }

        BWTEncoder bwt = new BWTEncoder();
        try {
            System.out.println(bwt.createBWT(text));
        } catch (IOException e) {
            System.err.println("Invalid Input");
            System.exit(1);
        }
    }
}
