import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.assertEquals;

public class StatableArrayTest {

    StatableArray<String> ra;

    @Before
    public void setUpStatableArray() throws LengthException {
        ra = new StatableArray<String>(50, "Jks");
    }

    @Test
    public void checkNewArrayRead() {
        assertEquals(0, ra.numberOfReads());
    }

    @Test
    public void checkNewArrayWrite() {
        assertEquals(0, ra.numberOfWrites());
    }

    @Test
    public void checkNewGet() {
        ra.get(3);
        ra.get(3);
        assertEquals(2, ra.numberOfReads());
        assertEquals(0, ra.numberOfWrites());
    }

    @Test
    public void checkNewPut() {
        ra.put(3, "Work");
        ra.put(4, "Work");
        ra.put(5, "Work");
        assertEquals(0, ra.numberOfReads());
        assertEquals(3, ra.numberOfWrites());
    }

    @Test
    public void checkNewLength() {
        ra.length();
        assertEquals(1, ra.numberOfReads());
        assertEquals(0, ra.numberOfWrites());
    }

    @Test
    public void checkResetStatistics() {
        ra.resetStatistics();
        assertEquals(0, ra.numberOfReads());
        assertEquals(0, ra.numberOfWrites());
    }
}
