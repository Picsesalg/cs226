/**
 * Alice Yang.
 * ayang36@jhu.edu
 * StatableArray.java
 * This program keeps track of each time a read/write is called on an abject.
 * @param <T> The arbitrary data type
 */
public class StatableArray<T> extends SimpleArray<T> implements Statable {

    private int read;
    private int write;

    /**
     * Default constructor for the Statable Array.
     * @param n The default length
     * @param t The default value
     */
    public StatableArray(int n, T t) {
        super(n, t);
        read = 0;
        write = 0;
    }

    @Override
    public void resetStatistics() {
        this.read = 0;
        this.write = 0;
    }

    @Override
    public int numberOfReads() {
        return this.read;
    }

    @Override
    public int numberOfWrites() {
        return this.write;
    }

    @Override
    public T get(int i) throws IndexException {
        this.read++;
        return super.get(i);
    }

    @Override
    public void put(int i, T t) throws IndexException {
        this.write++;
        super.put(i, t);
    }

    @Override
    public int length() {
        this.read++;
        return super.length();
    }
}
