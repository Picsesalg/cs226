/**
 * Alice Yang.
 * ayang36@jhu.edu
 * InsertionSort.java
 * @param <T> An abstract data type
 */
public class InsertionSort<T extends Comparable<T>>
    implements SortingAlgorithm<T> {

    /**
     * Tells you whether a is greather than b.
     * @param a The first value
     * @param b The second value
     * @return Whether a is greater than b
     */
    private boolean greaterThan(T a, T b) {
        return a.compareTo(b) > 0;
    }

    @Override
    public void sort(Array<T> array) {
        for (int i = 1; i < array.length(); i++) {
            T tested = array.get(i);
            int j = i - 1;
            while (j >= 0 && greaterThan(array.get(j), tested)) {
                array.put(j + 1, array.get(j));
                j--;
            }
            array.put(j + 1, tested);
        }
    }

    @Override
    public String name() {
        return "Insertion Sort";
    }
}
