/**
 * Alice Yang.
 * ayang36@jhu.edu
 * BubbleSort.java
 * @param <T> An arbitrary data type
 */
public class BubbleSort<T extends Comparable<T>>
    implements SortingAlgorithm<T> {

    /**
     * Sees if a is greather than b.
     * @param a The first value
     * @param b The second value
     * @return true if a is greater than b
     */
    private boolean greaterThan(T a, T b) {
        return a.compareTo(b) > 0;
    }

    /**
     * Swaps the values ra[i] and ra[j].
     * @param ra The array to be swapping in
     * @param i The first index
     * @param j The second index
     */
    private void swap(Array<T> ra, int i, int j) {
        T temp = ra.get(i);
        ra.put(i, ra.get(j));
        ra.put(j, temp);
    }

    @Override
    public void sort(Array<T> array) {
        int sumIt = array.length();
        int numIt = 0;
        int flag = 1;
        while (numIt < sumIt) {
            for (int i = 0; i < sumIt - numIt - 1; i++) {
                if (greaterThan(array.get(i), array.get(i + 1))) {
                    flag = 0;
                    swap(array, i, i + 1);
                }
            }
            if (flag == 1) {
                break;
            }
            numIt++;
        }
    }

    @Override
    public String name() {
        return "Bubble Sort";
    }
}
