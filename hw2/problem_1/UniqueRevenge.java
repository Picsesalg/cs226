/**
 * Alice Yang
 * ayang36@jhu.edu
 */

import java.util.Scanner;

/**
 * Program to read and print all the numbers in the input once.
 */
public final class UniqueRevenge {

    /**
     * To satisfy Checkstyle.
     */
    private UniqueRevenge() {}

    /**
     * Sees if a number exists in the array already.
     * @param array The Simple Array with the uniue integers stored
     * @param nextInt The next integer in question
     * @param index The index to put the next integer into
     * @return 0 if the number is already in the array. 1 if it's not
     */
    public static int findExisting(SimpleArray<Integer> array,
            int nextInt, int index) {

        for (int i = 0; i < index; i++) {
            if (nextInt == array.get(i)) {
                return 0;
            }
        }

        return 1;
    }

    /**
     * Increases the size of the array by 2x if it gets filled.
     * @param array The array with the unique integers in it
     * @param arraySize The size of the array
     * @param index The next empty array index
     * @return The new increased array
     */
    public static SimpleArray<Integer> increaseSize(SimpleArray<Integer> array,
            int arraySize, int index) {

        SimpleArray<Integer> temp = new SimpleArray<Integer>(arraySize, 0);

        for (int i = 0; i < arraySize; i++) {
            temp.put(i, array.get(i));
        }

        arraySize *= 2;

        array = new SimpleArray<Integer>(arraySize, 0);

        for (int i = 0; i < index; i++) {
            array.put(i, temp.get(i));
        }

        return array;
    }

    /**
     * The main program.
     * @param args The arguments in command line
     */
    public static void main(String[] args) {

        Scanner kb = new Scanner(System.in);
        int arraySize = 50; //Inital size of array
        SimpleArray<Integer> array = new SimpleArray<Integer>(arraySize, 0);
        int index = 0; //Keeps track of which index to put the new int in.
        int addNew = 1; //Turns to 0 if the number already exists.
        int nextInt = 0; //The next int to be read in.

        while (kb.hasNext()) {
            if (!kb.hasNextInt()) {
                throw new IllegalArgumentException("Only integers allowed.");
            }
            nextInt = kb.nextInt();
            if (index == 0) {
                array.put(index, nextInt);
                index++;
            } else {
                addNew = findExisting(array, nextInt, index);
                if (addNew == 1) {
                    if (index == arraySize) {
                        array = increaseSize(array, arraySize, index);
                        arraySize *= 2;
                    }
                    array.put(index, nextInt);
                    index++;
                }
            }
        }
        for (int i = 0; i < index; i++) {
            System.out.println(array.get(i));
        }
    }
}
