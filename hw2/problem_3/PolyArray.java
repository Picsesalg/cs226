/**
 * Alice Yang
 * ayang36@jhu.edu
 * PolyArray.java
 */
import java.util.ArrayList; // see note in main() below
import java.util.Iterator;

/**
    Simple polymorphic test framework for arrays.

    See last week's PolyCount. You need to add more test cases (meaning more
    methods like testNewLength and testNewWrongLength below) to make sure all
    preconditions and axioms are indeed as expected from the specification.
*/
public final class PolyArray {
    private static final int LENGTH = 113;
    private static final int INITIAL = 7;
    private static final int PUT = 20;

    private PolyArray() {}

    // methods for testing axioms go here

    private static void testNewLength(Array<Integer> a) {
        assert a.length() == LENGTH;
    }

    private static void testNewGet(Array<Integer> a) {
        for (int i = 0; i < LENGTH; i++) {
            assert a.get(i) == INITIAL;
        }
    }

    // TODO more test cases for axioms

    private static void testNewPut(Array<Integer> a) {
        for (int i = 0; i < LENGTH; i++) {
            a.put(i, PUT);
            assert a.get(i) == PUT;
        }
    }

    private static void testNewIterator(Array<Integer> a) {
        Iterator<Integer> it = a.iterator();
        int i = 0;
        int sum = 0;
        while (it.hasNext()) {
            sum += it.next();
        }
        System.out.println(sum);
        assert sum == PUT * LENGTH;
    }

    // methods for testing preconditions go here

    private static void testNewWrongLength() {
        try {
            Array<Integer> a = new SimpleArray<>(0, INITIAL);
            assert false;
        } catch (LengthException e) {
            // passed the test, nothing to do
        }
        // TODO the same for ListArray and SparseArray here
        try {
            Array<Integer> a = new ListArray<>(0, INITIAL);
        } catch (LengthException e) {
            //Nothing?
        }
        try {
            Array<Integer> a = new SimpleArray<>(0, INITIAL);
        } catch (LengthException e) {
            //Nothing?
        }
    }

    // TODO more test cases for preconditions
    private static void testNewWrongPut() {
        try {
            Array<Integer> a = new SimpleArray<>(LENGTH, INITIAL);
            a.put(LENGTH + 1, 20);
        } catch (IndexException e) {
            //Passed
        }
        try {
            Array<Integer> a = new ListArray<>(LENGTH, INITIAL);
            a.put(LENGTH + 1, 20);
        } catch (IndexException e) {
            //Passed
        }
        try {
            Array<Integer> a = new SparseArray<>(LENGTH, INITIAL);
            a.put(LENGTH + 1, 20);
        } catch (IndexException e) {
            //Passed
        }
    }

    private static void testNewWrongGet() {
        try {
            Array<Integer> a = new SimpleArray<>(LENGTH, INITIAL);
            a.get(LENGTH + 1);
        } catch (IndexException e) {
            //Passed
        }
        try {
            Array<Integer> a = new ListArray<>(LENGTH, INITIAL);
            a.get(LENGTH + 1);
        } catch (IndexException e) {
            //Passed
        }
        try {
            Array<Integer> a = new SparseArray<>(LENGTH, INITIAL);
            a.get(LENGTH + 1);
        } catch (IndexException e) {
            //Passed
        }
    }

    /**
        Run (mostly polymorphic) tests on various array implementations.

        Make sure you run this with -enableassertions! We'll learn a much
        better approach to unit testing later.

        @param args Command line arguments (ignored).
    */
    public static void main(String[] args) {
        // For various technical reasons, we cannot use a plain Java array here
        // like we did in PolyCount. Sorry.
        ArrayList<Array<Integer>> arrays = new ArrayList<>();
        arrays.add(new SimpleArray<Integer>(LENGTH, INITIAL));
        // TODO add ListArray and SparseArray instances here
        arrays.add(new ListArray<Integer>(LENGTH, INITIAL));
        arrays.add(new SparseArray<Integer>(LENGTH, INITIAL));
        // Test all the axioms. We can do that nicely in a loop. In the test
        // methods, keep in mind that you are handed the same object over and
        // over again!
        for (Array<Integer> a: arrays) {
            testNewLength(a);
            testNewGet(a);
            // TODO call more test cases (hint: order matters)
            testNewPut(a);
            testNewIterator(a);
        }

        // Test all the preconditions. Sadly we have to code each one of these
        // out manually, not even Java's reflection API would help...
        testNewWrongLength();
        // TODO call more test cases
        testNewWrongPut();
        testNewWrongGet();

        System.out.println("All tests passed");
    }
}
