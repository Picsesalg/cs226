/**
 * Alice Yang
 * ayang36@jhu.edu
 * SparseArray.java
 */
import java.util.Iterator;

/**
 * The SparseArray class for problem 3.
 * This program creates an "array" which is really a linked list in the backend.
 * If nothing is put into the array, then the
 * only output for each index should be the default value.
 * If something is put/altered into the element then
 * for that index, the return element for that index should
 * be changed into the value of the new put.
 * This object holds a node that's the head of the list, a default
 * size of the array, and the default value.
 *
 * The advantage of using the SparseArray is to minimise the amount of memory
 * utilised by the array, by only storing values that differ from the
 * default.
 * @param <T> The arbitrary type for SparseArray
 */
public class SparseArray<T> implements Array<T> {

    /**
     * A node for the linked list.
     * @param <T> The arbitrary type for Node
     */
    private static class Node<T> {
        int pos;
        T data;
        Node<T> next;
    }

    private Node<T> head;
    private int size;
    private T val;

    private class ArrayIterator implements Iterator<T> {

        int position;

        ArrayIterator() {}

        @Override
        public T next() {
            return SparseArray.this.get(position++);
        }

        @Override
        public boolean hasNext() {
            return position < SparseArray.this.length();
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * The SparseArray constructor.
     * @param i The default size of the array
     * @param defaultValue The default value and data type for the array
     * @throws LengthException Ensures the length is positive
     */
    public SparseArray(int i, T defaultValue) throws LengthException {
        if (i <= 0) {
            throw new LengthException();
        }
        this.size = i;
        this.val = defaultValue;
    }

    @Override
    public void put(int i, T t) throws IndexException {
        if (i < 0 || i >= this.size) {
            throw new IndexException();
        }
        ArrayIterator it = new ArrayIterator();
        Node<T> n = new Node<T>();
        Node<T> cur = this.head;

        n.pos = i;
        n.data = t;
        n.next = null;

        if (this.head == null) {
            this.head = n;
            return;
        }

        while (cur != null) {
            if (cur.pos == i) {
                cur.data = t;
                return;
            }
            cur = cur.next;
        }
        /**if (this.cur.pos == i) {
            this.cur.data = t;
            return;
        }

        while (it.hasNext()) {
            if (it.cur.pos == i) {
                it.cur.data = t;
            }
            it.cur = it.cur.next;
        }*/
        n.next = this.head;
        this.head = n;
    }

    @Override
    public T get(int i) throws IndexException {
        if (i < 0 || i >= this.size) {
            throw new IndexException();
        }
        Node<T> cur = this.head;
        /**ArrayIterator it = new ArrayIterator();*/

        if (this.head == null) {
            return this.val;
        }
        while (cur != null) {
            if (cur.pos == i) {
                return cur.data;
            }
            cur = cur.next;
        }
        /**
        while (it.hasNext()) {
            if (it.cur.pos == i) {
                return it.cur.data;
            }
            it.cur = it.cur.next;
        }
        if (it.cur.pos == i) {
            return it.cur.data;
        }*/
        return this.val;
    }

    @Override
    public int length() {
        return this.size;
    }

    @Override
    public Iterator<T> iterator() {
        return new ArrayIterator();
    }

    /**
     * The main program.
     * @param args The input from the command line
     */
    public static void main(String[] args) {
        SparseArray<String> ra = new SparseArray<String>(5, "Calvin");

        ra.put(2, "Alice");
        ra.put(2, "Elizabeth");
        ra.put(3, "Greay");
        ra.put(4, "Geezus");
        ra.put(4, "God fuck");

        for (String a: ra) {
            System.out.println(a);
        }
    }
}
