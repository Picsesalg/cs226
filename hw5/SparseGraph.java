/**
 * Alice Yang
 * ayang36@jhu.edu
 * SparseGraph.java
 */

import java.util.ArrayList;

/**
 * Graph implementation.
 * @param <V> Vertex element type
 * @param <E> Edge element type
 */
public class SparseGraph<V, E> implements Graph<V, E> {

    /**
     * A specific implementation of the vertex interface.
     * @param <T> Vertex element type
     */
    private class SpecVertex<T> implements Vertex<T> {

        Object label;
        T data;
        ArrayList<Edge<E>> incoming;
        ArrayList<Edge<E>> outgoing;

        @Override
        public T get() {
            return this.data;
        }

        @Override
        public void put(T t) {
            this.data = t;
        }
    }

    /**
     * A specific implementation of the edge interface.
     * @param <T> The edge element type
     */
    private class SpecEdge<T> implements Edge<T> {

        Object label;
        T data;
        Vertex<V> fromVertex;
        Vertex<V> toVertex;

        @Override
        public T get() {
            return this.data;
        }

        @Override
        public void put(T t) {
            this.data = t;
        }
    }

    ArrayList<Vertex<V>> vertexList;
    ArrayList<Edge<E>> edgeList;

    /**
     * SparseGraph constructor.
     */
    public SparseGraph() {
        this.vertexList = new ArrayList<Vertex<V>>();
        this.edgeList = new ArrayList<Edge<E>>();
    }

    /**
     * Convertes Vertex to SpecVertex.
     * @param <V> Vertex type
     * @param v The Vertex to be converted
     * @return The converted SpecVertex
     */
    private SpecVertex<V> vertexConvert(Vertex<V> v) {
        SpecVertex<V> converted = (SpecVertex<V>) v;
        return converted;
    }

    /**
     * COnvertes Edge to SpecEdge.
     * @param <E> Edge type
     * @param e The Edge to be converted
     * @return The converted SpecEdge
     */
    private SpecEdge<E> edgeConvert(Edge<E> e) {
        SpecEdge<E> converted = (SpecEdge<E>) e;
        return converted;
    }

    @Override
    public Vertex<V> insert(V v) {

        SpecVertex<V> inserting = new SpecVertex<V>();

        inserting.label = null;
        inserting.data = v;
        inserting.incoming = new ArrayList<Edge<E>>();
        inserting.outgoing = new ArrayList<Edge<E>>();

        this.vertexList.add(inserting);

        return inserting;
    }

    @Override
    public Edge<E> insert(Vertex<V> from, Vertex<V> to, E e)
        throws PositionException, InsertionException {

        SpecEdge<E> inserting = new SpecEdge<E>();

        //PositionException
        int fromIndex = this.vertexList.indexOf(from);
        int toIndex = this.vertexList.indexOf(to);
        if (fromIndex == -1 || toIndex == -1) {
            throw new PositionException();
        }

        //InsertionException
        if (fromIndex == toIndex) {
            throw new InsertionException();
        }

        inserting.label = null;
        inserting.data = e;
        inserting.fromVertex = from;
        inserting.toVertex = to;

        int edgeIndex = this.edgeList.indexOf(inserting);

        //Duplicate edge
        for (Edge<E> edge : this.outgoing(from)) {
            SpecEdge<E> specEdge = edgeConvert(edge);
            if (specEdge.toVertex.equals(to)) {
                throw new InsertionException();
            }
        }

        this.edgeList.add(inserting);
        SpecVertex<V> fromVertex = vertexConvert(
            this.vertexList.get(fromIndex));
        fromVertex.outgoing.add(inserting);
        SpecVertex<V> toVertex = vertexConvert(this.vertexList.get(toIndex));
        toVertex.incoming.add(inserting);

        return inserting;
    }

    @Override
    public V remove(Vertex<V> v) throws PositionException, RemovalException {

        int vertexIndex = this.vertexList.indexOf(v);

        //PositionException
        if (vertexIndex == -1) {
            throw new PositionException();
        }

        //RemovalException
        SpecVertex<V> removing = vertexConvert(
            this.vertexList.get(vertexIndex));
        if (!removing.incoming.isEmpty()) {
            throw new RemovalException();
        }
        if (!removing.outgoing.isEmpty()) {
            throw new RemovalException();
        }

        this.vertexList.remove(removing);

        return removing.data;
    }

    @Override
    public E remove(Edge<E> e) throws PositionException {

        int edgeIndex = this.edgeList.indexOf(e);

        //PositionException
        if (edgeIndex == -1) {
            throw new PositionException();
        }

        SpecEdge<E> removing = edgeConvert(this.edgeList.get(edgeIndex));

        this.edgeList.remove(removing);
        int fromIndex = this.vertexList.indexOf(
            removing.fromVertex);
        SpecVertex<V> fromVertex = vertexConvert(
            this.vertexList.get(fromIndex));
        fromVertex.outgoing.remove(e);
        int toIndex = this.vertexList.indexOf(removing.toVertex);
        SpecVertex<V> toVertex = vertexConvert(this.vertexList.get(toIndex));
        toVertex.incoming.remove(e);

        return removing.data;
    }

    @Override
    public Iterable<Vertex<V>> vertices() {

        ArrayList<Vertex<V>> cloned = new ArrayList<Vertex<V>>();

        for (int i = 0; i < this.vertexList.size(); i++) {
            cloned.add(this.vertexList.get(i));
        }

        return cloned;
    }

    @Override
    public Iterable<Edge<E>> edges() {

        ArrayList<Edge<E>> cloned = new ArrayList<Edge<E>>();

        for (int i = 0; i < this.edgeList.size(); i++) {
            cloned.add(this.edgeList.get(i));
        }

        return cloned;
    }

    @Override
    public Iterable<Edge<E>> outgoing(Vertex<V> v) throws PositionException {

        ArrayList<Edge<E>> cloned = new ArrayList<Edge<E>>();
        int vertexIndex = this.vertexList.indexOf(v);

        //PositionException
        if (vertexIndex == -1) {
            throw new PositionException();
        }

        SpecVertex<V> vertex = vertexConvert(this.vertexList.get(vertexIndex));

        for (int i = 0; i < vertex.outgoing.size(); i++) {
            cloned.add(vertex.outgoing.get(i));
        }

        return cloned;
    }

    @Override
    public Iterable<Edge<E>> incoming(Vertex<V> v) throws PositionException {

        ArrayList<Edge<E>> cloned = new ArrayList<Edge<E>>();
        int vertexIndex = this.vertexList.indexOf(v);

        //PositionException
        if (vertexIndex == -1) {
            throw new PositionException();
        }

        SpecVertex<V> vertex = vertexConvert(this.vertexList.get(vertexIndex));

        for (int i = 0; i < vertex.incoming.size(); i++) {
            cloned.add(vertex.incoming.get(i));
        }

        return cloned;
    }

    @Override
    public Vertex<V> from(Edge<E> e) throws PositionException {

        int edgeIndex = this.edgeList.indexOf(e);

        //PositionException
        if (edgeIndex == -1) {
            throw new PositionException();
        }

        SpecEdge<E> edge = edgeConvert(this.edgeList.get(edgeIndex));

        return edge.fromVertex;
    }

    @Override
    public Vertex<V> to(Edge<E> e) throws PositionException {

        int edgeIndex = this.edgeList.indexOf(e);

        //PositionException
        if (edgeIndex == -1) {
            throw new PositionException();
        }

        SpecEdge<E> edge = edgeConvert(this.edgeList.get(edgeIndex));

        return edge.toVertex;
    }

    @Override
    public void label(Vertex<V> v, Object l) throws PositionException {

        int vertexIndex = this.vertexList.indexOf(v);

        //PositionException
        if (vertexIndex == -1) {
            throw new PositionException();
        }

        SpecVertex<V> vertex = vertexConvert(this.vertexList.get(vertexIndex));
        vertex.label = l;
    }

    @Override
    public void label(Edge<E> e, Object l) throws PositionException {

        int edgeIndex = this.edgeList.indexOf(e);

        //PositionException
        if (edgeIndex == -1) {
            throw new PositionException();
        }

        SpecEdge<E> graphEdge = edgeConvert(this.edgeList.get(edgeIndex));
        graphEdge.label = l;

        Vertex<V> fromV = graphEdge.fromVertex;
        Vertex<V> toV = graphEdge.toVertex;
        int vertexIndex = this.vertexList.indexOf(fromV);
        SpecVertex<V> fromVertex = vertexConvert(
            this.vertexList.get(vertexIndex));
        edgeIndex = fromVertex.outgoing.indexOf(e);
        graphEdge = edgeConvert(fromVertex.outgoing.get(edgeIndex));
        graphEdge.label = l;

        vertexIndex = this.vertexList.indexOf(toV);
        SpecVertex<V> toVertex = vertexConvert(
            this.vertexList.get(vertexIndex));
        edgeIndex = toVertex.incoming.indexOf(e);
        graphEdge = edgeConvert(toVertex.incoming.get(edgeIndex));
        graphEdge.label = l;
    }

    @Override
    public Object label(Vertex<V> v) throws PositionException {

        int vertexIndex = this.vertexList.indexOf(v);

        //PositionException
        if (vertexIndex == -1) {
            throw new PositionException();
        }

        SpecVertex<V> labelling = vertexConvert(
            this.vertexList.get(vertexIndex));

        return labelling.label;
    }

    @Override
    public Object label(Edge<E> e) throws PositionException {

        int edgeIndex = this.edgeList.indexOf(e);

        //POsitionException
        if (edgeIndex == -1) {
            throw new PositionException();
        }

        SpecEdge<E> labelling = edgeConvert(this.edgeList.get(edgeIndex));

        return labelling.label;
    }

    @Override
    public void clearLabels() {

        SpecVertex<V> vertex;
        SpecEdge<E> edge;

        for (int i = 0; i < this.vertexList.size(); i++) {
            vertex = vertexConvert(this.vertexList.get(i));
            vertex.label = null;
        }

        for (int i = 0; i < this.edgeList.size(); i++) {
            edge = edgeConvert(this.edgeList.get(i));
            edge.label = null;
        }
    }

    /**
     * Creates a formatted string for digraph.
     * @return The formatted string
     */
    public String toString() {

        String print = "digraph {\n";

        for (Vertex<V> v : this.vertices()) {
            print += "  \"";
            print += v.get();
            print += "\";\n";
        }

        for (Vertex<V> v : this.vertices()) {
            for (Edge<E> e : this.outgoing(v)) {
                SpecEdge<E> edge = edgeConvert(e);
                print += "  \"";
                print += edge.fromVertex.get();
                print += "\" -> \"";
                print += edge.toVertex.get();
                print += "\" [label=\"";
                print += edge.data;
                print += "\"];\n";
            }
        }

        print += "}";

        return print;
    }

}
