/**
 * Alice Yang
 * ayang36@jhu.edu
 * SparseGraphTest.java
 */

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.assertEquals;

public class SparseGraphTest {

    SparseGraph<String, Integer> tester;
    SparseGraph<String, Integer> tester2;

    @Before
    public void setUpSparseGraph() {
        tester = new SparseGraph<String, Integer>();
        tester2 = new SparseGraph<String, Integer>();
    }

    @Test
    public void newInsertVertex() {
        Vertex<String> zero = tester.insert("0");
        assertEquals("0", zero.get());
    }

    @Test
    public void insertVertex01() {
        Vertex<String> zero = tester.insert("0");
        Vertex<String> one = tester.insert("1");
        assertEquals("0", zero.get());
        assertEquals("1", one.get());
    }

    @Test
    public void newEdgeInsert() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 0);
        assertEquals((Integer) 0, zeroE.get());
    }

    @Test
    public void edgeInsert01() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 0);
        Edge<Integer> oneE = tester.insert(oneV, zeroV, 0);
        assertEquals((Integer) 0, zeroE.get());
        assertEquals((Integer) 0, oneE.get());
    }

    @Test
    public void edgeInsert02() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Vertex<String> twoV = tester.insert("two");
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 0);
        Edge<Integer> oneE = tester.insert(twoV, oneV, 1);
        assertEquals((Integer) 0, zeroE.get());
        assertEquals((Integer) 1, oneE.get());
    }

    @Test
    public void edgeInsert03() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Vertex<String> twoV = tester.insert("two");
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 0);
        Edge<Integer> oneE = tester.insert(twoV, zeroV, 1);
        for (Edge<Integer> e : tester.outgoing(zeroV)) {
            assertEquals((Integer) 0, e.get());
        }
        for (Edge<Integer> e : tester.incoming(zeroV)) {
            assertEquals((Integer) 1, e.get());
        }
        for (Edge<Integer> e : tester.incoming(oneV)) {
            assertEquals((Integer) 0, e.get());
        }
        for (Edge<Integer> e : tester.outgoing(twoV)) {
            assertEquals((Integer) 1, e.get());
        }
    }
    
    @Test(expected=PositionException.class)
    public void edgeInsertException01() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Vertex<String> twoV = tester.insert("two");
        tester.remove(zeroV);
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 0); 
    }

    @Test(expected=PositionException.class)
    public void edgeInsertException02() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Vertex<String> twoV = tester.insert("two");
        tester.remove(oneV);
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 0);
    }

    @Test(expected=InsertionException.class)
    public void edgeInsertionException03() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Vertex<String> twoV = tester.insert("two");
        Edge<Integer> zeroE = tester.insert(zeroV, zeroV, 0);
    }

    @Test(expected=InsertionException.class)
    public void edgeInsertionException04() {
        Vertex<String> zeroV = tester.insert("0");
        Vertex<String> oneV = tester.insert("1");
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 1);
        Edge<Integer> oneE = tester.insert(zeroV, oneV, 12);
    }

    @Test(expected=PositionException.class)
    public void removeVertexException01() {
        Vertex<String> zeroV = tester.insert("0");
        tester.remove(zeroV);
        tester.remove(zeroV);
    }

    @Test(expected=RemovalException.class)
    public void removeVertexException02() {
        Vertex<String> zeroV = tester.insert("0");
        Vertex<String> oneV = tester.insert("1");
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 1);
        tester.remove(zeroV);
    }

    @Test(expected=RemovalException.class)
    public void removeVertexException03() {
        Vertex<String> zeroV = tester.insert("0");
        Vertex<String> oneV = tester.insert("1");
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 1);
        tester.remove(oneV);
    }

    @Test(expected=PositionException.class)
    public void edgeRemovalException01() {
        Vertex<String> zeroV = tester.insert("0");
        Vertex<String> oneV = tester.insert("1");
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 1);
        tester.remove(zeroE);
        tester.remove(zeroE);
    }

    @Test
    public void vertexIterable01() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Vertex<String> twoV = tester.insert("two");
        int sum = 0;
        for (Vertex<String> v: tester.vertices()) {
            sum++;
            if (v.equals(zeroV)) {
                assertEquals("zero", v.get());
            }
            if (v.equals(oneV)) {
                assertEquals("one", v.get());
            }
            if (v.equals(twoV)) {
                assertEquals("two", v.get());
            }
        }
        assertEquals(3, sum);
    }

    @Test
    public void vertexIterable02() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Vertex<String> twoV = tester.insert("two");
        tester.remove(zeroV);
        tester.remove(twoV);
        int sum = 0;
        for (Vertex<String> v: tester.vertices()) {
            sum++;
        }
        assertEquals(1, sum);
    }

    @Test
    public void edgeIterable01() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 0);
        Edge<Integer> oneE = tester.insert(oneV, zeroV, 1);
        int sum = 0;
        for (Edge<Integer> e : tester.edges()) {
            sum++;
            if (e.equals(zeroE)) {
                assertEquals((Integer) 0, e.get());
            }
            if (e.equals(oneE)) {
                assertEquals((Integer) 1, e.get());
            }
        }
        assertEquals(2, sum);
    }

    @Test
    public void edgeIterable02() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Vertex<String> twoV = tester.insert("two");
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 0);
        Edge<Integer> twoE = tester.insert(oneV, twoV, 2);
        Edge<Integer> threeE = tester.insert(zeroV, twoV, 3);
        tester.remove(threeE);
        int sum = 0;
        for (Edge<Integer> e: tester.edges()) {
            sum++;
        }
        assertEquals(2, sum);
    }

    @Test
    public void outgoingEdgeIterable01() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Vertex<String> twoV = tester.insert("two");
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 0);
        Edge<Integer> threeE = tester.insert(zeroV, twoV, 3);
        int sum = 0;
        for (Edge<Integer> e: tester.outgoing(zeroV)) {
            sum++;
        }
        assertEquals(2, sum);
    }

    @Test
    public void outgoingEdgeIterable02() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Vertex<String> twoV = tester.insert("two");
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 0);
        Edge<Integer> threeE = tester.insert(zeroV, twoV, 3);
        tester.remove(threeE);
        int sum = 0;
        for (Edge<Integer> e: tester.outgoing(zeroV)) {
            sum++;
        }
        assertEquals(1, sum);
    }

    @Test(expected=PositionException.class)
    public void outgoingEdgeIterableException() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("1");
        tester.remove(oneV);
        Iterable<Edge<Integer>> it = tester.outgoing(oneV); 
    }

    @Test
    public void incomingEdgeIterable01() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Vertex<String> twoV = tester.insert("two");
        Edge<Integer> twoE = tester.insert(oneV, twoV, 2);
        Edge<Integer> threeE = tester.insert(zeroV, twoV, 3);
        int sum = 0;
        for (Edge<Integer> e: tester.incoming(twoV)) {
            sum++;
        }
        assertEquals(2, sum); 
    }

    @Test
    public void incomingEdgeIterable02() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Vertex<String> twoV = tester.insert("two");
        Edge<Integer> twoE = tester.insert(oneV, twoV, 2);
        Edge<Integer> threeE = tester.insert(zeroV, twoV, 3);
        tester.remove(threeE);
        int sum = 0;
        for (Edge<Integer> e: tester.incoming(twoV)) {
            sum++;
        }
        assertEquals(1, sum);
    }

    @Test(expected=PositionException.class)
    public void incomingEdgeIterableException01() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Vertex<String> twoV = tester.insert("two");
        tester.remove(twoV);
        for (Edge<Integer> e: tester.incoming(twoV)) {
        }
    }

    @Test
    public void fromVertex01() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 0);
        Vertex<String> v = tester.from(zeroE);
        assertEquals(zeroV.get(), v.get());
    }

    @Test(expected=PositionException.class)
    public void fromVertexException01() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 0);
        tester.remove(zeroE);
        Vertex<String> v = tester.from(zeroE);
    }

    @Test
    public void toVertex01() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 0);
        Vertex<String> v = tester.to(zeroE);
        assertEquals(oneV.get(), v.get());
    }

    @Test(expected=PositionException.class)
    public void toVertexException() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 0);
        tester.remove(zeroE);
        Vertex<String> v = tester.to(zeroE);
    }

    @Test
    public void labelVertex01() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Vertex<String> twoV = tester.insert("two");
        tester.label(zeroV, "ZERO");
        Object l = tester.label(zeroV);
        assertEquals("ZERO", l);
    }

    @Test
    public void labelVertex02() {
        Vertex<String> zeroV = tester.insert("zero");
        Object l = tester.label(zeroV);
        assertEquals(null, l);
    }

    @Test(expected=PositionException.class)
    public void labelVertexException01() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Vertex<String> twoV = tester.insert("two");
        tester.remove(zeroV);
        tester.label(zeroV, "ZERO");
    }

    @Test(expected=PositionException.class)
    public void labelVertexException02() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Vertex<String> twoV = tester.insert("two");
        tester.label(zeroV, "ZERO");
        tester.remove(zeroV);
        Object l = tester.label(zeroV);
    }

    @Test
    public void labelEdge01() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 0);
        tester.label(zeroE, "ZERO");
        Object l = tester.label(zeroE);
        assertEquals("ZERO", l);
    }

    @Test
    public void labelEdge02() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 0);
        Object l = tester.label(zeroE);
        assertEquals(null, l);
    }

    @Test(expected=PositionException.class)
    public void labelEdgeException01() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 0);
        tester.remove(zeroE);
        tester.label(zeroE, "ZERO");
    }

    @Test(expected=PositionException.class)
    public void labelEdgeException02() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 0);
        tester.label(zeroE, "ZERO");
        tester.remove(zeroE);
        tester.label(zeroE);
    }

    @Test
    public void clearingLabels() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        Edge<Integer> zeroE = tester.insert(zeroV, oneV, 0);
        Edge<Integer> oneE = tester.insert(oneV, zeroV, 1);
        tester.label(zeroV, "ZERO");
        tester.label(oneV, "ZERO");
        tester.label(zeroE, "ZERO");
        tester.label(oneE, "ZERO");
        tester.clearLabels();
        for (Vertex<String> v : tester.vertices()) {
            Object l = tester.label(v);
            assertEquals(null, l);
        }
        for (Edge<Integer> e : tester.edges()) {
            Object l = tester.label(e);
            assertEquals(null, l);
        }
    }

    @Test(expected=PositionException.class)
    public void incorrectOwner() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester2.insert("one");
        tester.insert(zeroV, oneV, 0);
    }

    @Test
    public void testToString() {
        Vertex<String> zeroV = tester.insert("zero");
        Vertex<String> oneV = tester.insert("one");
        tester.insert(zeroV, oneV, 0);
        String a = "digraph {\n";
        a += "  \"zero\";\n";
        a += "  \"one\";\n";
        a += "  \"zero\" -> \"one\" [label=\"0\"];\n";
        a += "}";
        assertEquals(a, tester.toString());
    }
}
