import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.assertEquals;

public class TestArrayDequeue {
    
    ArrayDequeue<String> deck;

    @Before
    public void setUpArrayDequeue() throws LengthException {
        deck = new ArrayDequeue<String>();
    }

    @Test
    public void checkNewEmpty() {
        assertEquals(true, deck.empty());
    }

    @Test
    public void checkEMpty() {
        deck.insertFront("1");
        assertEquals(false, deck.empty());
    }

    @Test
    public void checkNewLength() {
        assertEquals(0, deck.length());
    }

    @Test
    public void checkLength() {
        deck.insertFront("1");
        deck.insertFront("2");
        deck.insertFront("3");
        assertEquals(3, deck.length());
    }

    @Test
    public void checkNewToString() {
        assertEquals("[]", deck.toString());
    }

    @Test
    public void checkToString() {
        deck.insertBack("1");
        deck.insertFront("2");
        assertEquals("[2, 1]", deck.toString());
    }

    @Test
    public void checkFrontandInsertFront() {
        deck.insertFront("1");
        deck.insertFront("2");
        deck.insertFront("3");
        assertEquals("3", deck.front());
    }

    @Test(expected=EmptyException.class)
    public void checkNewFrontEmpty() {
        deck.front();
    }

    @Test
    public void checkBackandInsertBack() {
        deck.insertBack("3");
        deck.insertBack("2");
        deck.insertBack("1");
        assertEquals("1", deck.back());
    }

    @Test(expected=EmptyException.class)
    public void checkNewBackEmpty() {
        deck.back();
    }

    @Test
    public void checkInsertFrontThenBack() {
        deck.insertFront("1");
        deck.insertBack("2");
        assertEquals("1", deck.front());
        assertEquals("2", deck.back());
    }

    @Test
    public void checkInsertBackThenFront() {
        deck.insertBack("1");
        deck.insertFront("2");
        assertEquals("1", deck.back());
        assertEquals("2", deck.front());
    }

    @Test
    public void checkRandomInsertsAndRemoves() {
        deck.insertFront("1");
        deck.removeFront();
        deck.insertBack("2");
        deck.insertBack("3");
        deck.insertBack("4");
        deck.removeBack();
        assertEquals("2", deck.front());
        assertEquals("3", deck.back());
    }

    @Test
    public void checkRemoveFront() {
        deck.insertFront("1");
        deck.insertFront("2");
        deck.removeFront();
        assertEquals("1", deck.front());
    }

    @Test(expected=EmptyException.class)
    public void checkNewRemoveFrontEmpty() {
        deck.removeFront();
    }

    @Test(expected=EmptyException.class)
    public void checkRemoveFrontEmpty() {
        deck.insertBack("1");
        deck.insertBack("2");
        deck.removeFront();
        deck.removeFront();
        deck.removeFront();
    }

    @Test
    public void checkRemoveBack() {
        deck.insertBack("1");
        deck.insertBack("2");
        deck.removeBack();
        assertEquals("1", deck.back());
    }

    @Test(expected=EmptyException.class)
    public void checkNewRemoveBackEmpty() {
        deck.removeBack();
    }

    @Test(expected=EmptyException.class)
    public void checkRemoveBackEmpty() {
        deck.insertFront("1");
        deck.insertBack("2");
        deck.removeBack();
        deck.removeBack();
        deck.removeBack();
    }
}
