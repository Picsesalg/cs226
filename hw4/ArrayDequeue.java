/**
 * Alice Yang
 * ayang36@jhu.edu
 * ArrayDequeue.java
 */

/**
 * ArrayDequeue class.
 * @param <T> The arbitrary data type
 */
public class ArrayDequeue<T> implements Dequeue<T> {

    int size;
    int elements;
    int frontIndex;
    int backIndex;
    T[] dequeue;

    /**
     * The default constructor.
     */
    public ArrayDequeue() {
        this.size = 1;
        this.elements = 0;
        this.frontIndex = -1;
        this.backIndex = -1;
        this.dequeue = (T[]) new Object[size];
    }

    @Override
    public boolean empty() {
        return this.elements == 0;
    }

    @Override
    public int length() {
        return this.elements;
    }

    @Override
    public T front() throws EmptyException {
        if (this.elements == 0) {
            throw new EmptyException();
        }
        return this.dequeue[this.frontIndex];
    }

    @Override
    public T back() throws EmptyException {
        if (this.elements == 0) {
            throw new EmptyException();
        }
        return this.dequeue[this.backIndex];
    }

    @Override
    public void insertFront(T t) {
        //Increase the size of the array dequeue
        if (this.elements + 1 > this.size) {
            this.increaseSize();
        }
        //First element in
        if (this.frontIndex == -1) {
            this.frontIndex = 0;
            this.backIndex = 0;
        } else {
            this.frontIndex = (this.frontIndex - 1) % this.size;
            if (this.frontIndex < 0) {
                this.frontIndex += this.size;
            }
        }
        this.dequeue[this.frontIndex] = t;
        this.elements++;
    }

    @Override
    public void insertBack(T t) {
        //Increase the size of the array dequeue
        if (this.elements + 1 > this.size) {
            this.increaseSize();
        }
        //First element in
        if (this.frontIndex == -1) {
            this.frontIndex = 0;
            this.backIndex = 0;
        } else {
            this.backIndex = (this.backIndex + 1) % this.size;
        }
        this.dequeue[this.backIndex] = t;
        this.elements++;
    }

    /**
     * Increases the size of the array.
     */
    public void increaseSize() {
        T[] temp = (T[]) new Object[this.size * 2];
        int j = this.frontIndex;
        //Copying the current array into the new array with indices
        //shifted for with front at index 0
        for (int i = 0; i < this.size; i++) {
            temp[i] = this.dequeue[j];
            j = (j + 1) % this.size;
        }
        this.size *= 2;
        this.dequeue = (T[]) new Object[this.size];
        //Copy from temp into dequeue
        for (int i = 0; i < this.elements; i++) {
            this.dequeue[i] = temp[i];
        }
        this.frontIndex = 0;
        this.backIndex = this.elements - 1;
    }

    @Override
    public void removeFront() throws EmptyException {
        if (this.elements == 0) {
            throw new EmptyException();
        }
        this.dequeue[this.frontIndex] = null;
        this.elements--;
        this.frontIndex = (this.frontIndex + 1) % this.size;
    }

    @Override
    public void removeBack() throws EmptyException {
        if (this.elements == 0) {
            throw new EmptyException();
        }
        this.dequeue[this.backIndex] = null;
        this.elements--;
        this.backIndex = (this.backIndex - 1) % this.size;
        if (this.backIndex < 0) {
            this.backIndex += this.size;
        }
    }

    @Override
    public String toString() {
        String s = "";
        s += "[";
        int j = this.frontIndex;
        for (int i = 1; i < this.elements; i++) {
            j = j % this.size;
            s += this.dequeue[j].toString();
            s += ", ";
            j++;
        }
        if (this.backIndex > -1) {
            s += this.dequeue[this.backIndex].toString();
        }
        s += "]";
        return s;
    }
}
