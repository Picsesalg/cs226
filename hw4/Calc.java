/**
 * Alice Yang
 * ayang36@jhu.edu
 * Calc.java
 */

import java.util.Scanner;

/**
 * Program to do basic arithmetic based on RPN.
 * @param <T> An arbitrary data type
 */
public class Calc<T> extends ListStack<T> {

    /**
     * The default constructor for Calc.
     */
    public Calc() {
        super();
    }

    /**
     * Determines whether or not an input is valid.
     * @param input the input in question
     * @return an int gt 0 if valid and that int corresponds
     to the switch statement, 0 if not
     */
    static int validInput(String input) {

        //If the input is an operator or ? . !
        switch (input) {
            case "?":
                return 2;
            case ".":
                return 3;
            case "!":
                return 4;
            case "+":
                return 5;
            case "-":
                return 6;
            case "*":
                return 7;
            case "/":
                return 8;
            case "%":
                return 9;
            default:
                break;
        }

        //If it's not check if it's an integer
        try {
            Integer.parseInt(input);
            return 1;
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    /**
     * Do the operations and catch any errors and ignore them.
     * @param input The operator in question
     * @param calculate The stack
     */
    static void operate(String input, Calc<Integer> calculate) {
        //Store the result here
        Integer result;

        //Making sure that there is at least 1 value in the stack
        if (calculate.empty()) {
            System.err.println("?Not enough inputs for operation.");
            return;
        }
        Integer firstInt = calculate.top();
        calculate.pop();

        //Making sure that there's at least 2 values in the stack
        if (calculate.empty()) {
            calculate.push(firstInt);
            System.err.println("?Not enough inputs for operation.");
            return;
        }
        Integer secondInt = calculate.top();
        calculate.pop();

        //Determining which operator to use
        switch (input) {
            case "+":
                result = secondInt + firstInt;
                calculate.push(result);
                return;
            case "-":
                result = secondInt - firstInt;
                calculate.push(result);
                return;
            case "*":
                result = secondInt * firstInt;
                calculate.push(result);
                return;
            case "/":
                if (firstInt == 0) {
                    calculate.push(secondInt);
                    calculate.push(firstInt);
                    System.err.println("?Can't divide by 0.");
                    return;
                }
                result = secondInt / firstInt;
                calculate.push(result);
                return;
            case "%":
                if (firstInt == 0) {
                    calculate.push(secondInt);
                    calculate.push(firstInt);
                    System.err.println("?Can't divide by 0.");
                    return;
                }
                result = secondInt % firstInt;
                calculate.push(result);
                return;
            default:
                System.err.println("?Invalid operator.");
                return;
        }
    }

    static void fullStop(Calc<Integer> calculate) {
        if (calculate.empty()) {
            System.err.println("?The stack is empty.");
            return;
        }
        Integer temp3 = calculate.top();
        calculate.pop();
        System.out.println(temp3);
        return;
    }

    /**
     * Determine what to do depending on the operator
     * @param validOrNot What type of input it is
     * @param input The actual input itself
     * @param calculate The stack
     */
    static void whatToDo(int validOrNot,
             String input, Calc<Integer> calculate) {
        //Determining the type of input each "word" is
        switch (validOrNot) {
            //Invalid input
            case 0:
                System.err.println("?Input not valid.");
                return;
            //If it's an integer
            case 1:
                int temp1 = Integer.parseInt(input);
                calculate.push(temp1);
                return;
            //? Printing
            case 2:
                System.out.println(calculate.toString());
                return;
            //. Pop and print top
            case 3:
                fullStop(calculate);
                return;
            //! Exit the program
            case 4:
                System.exit(0);
                return;
            //Operators
            default:
                operate(input, calculate);
                return;
        }
    }

    /**
     * The main method.
     * @param args The arguments from the command line
     */
    public static void main(String[] args) {

        Scanner kb = new Scanner(System.in);

        Calc<Integer> calculate = new Calc<Integer>();
        String input = "";
        int validOrNot = 0;

        while (kb.hasNext()) {
            input = kb.next();
            validOrNot = validInput(input);
            whatToDo(validOrNot, input, calculate);
        }
    }
}
